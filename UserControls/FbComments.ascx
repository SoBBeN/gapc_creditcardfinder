﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FbComments.ascx.cs" Inherits="FbComments" %>
<script type="text/javascript">
    function changeFaceBookCommentsURL(newurl) {
        document.getElementById("fbCommentsPlaceholder").innerHTML = '<div class="fb-comments" data-href="http://thepersonalfinancialguide.com/' + newurl + '" data-width="<%=dataWidth %>" data-num-posts="<%=dataNumPosts %>"></div>';
        FB.XFBML.parse();
    }
</script>
<div style="margin: 0 auto 0 auto; width:<%=dataWidth %>px;" id="fbCommentsPlaceholder">
    <div class="fb-comments" data-href="<%=dataHref %>" data-width="<%=dataWidth %>" data-num-posts="<%=dataNumPosts %>"></div>
</div>