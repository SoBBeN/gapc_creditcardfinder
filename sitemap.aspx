﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sitemap.aspx.cs" Inherits="sitemap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://ogp.me/ns/fb#">
<head runat="server">
    <asp:Literal runat="server" ID="lithead" />

    <meta property="og:site_name" content="<%=System.Configuration.ConfigurationManager.AppSettings["title"] %>" />
    <meta name="keywords" content="found,money,guide, money guide" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css' />
    <link href="/css/reset.css" rel="stylesheet" />
    <link href="/css/style.css" rel="stylesheet" />
    <link href="/css/sitemap.css" rel="stylesheet" />

    <script type="text/javascript">
        function togglecat() {
            $("#header .greentext").toggle();
            $("#categories").toggle();
        }
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <div id="headline">
            <img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/title.png" />
            <div class="signup"><a target="_blank" href="http://reg.thepersonalfinancialguide.com/hosting/staticpages/PFG_formCL.aspx?c=PFGCL&redir=0&referid=Content">Sign Up!</a></div>
        </div>
        <div id="header">
            <div class="logo">
                <img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/logo.png" /></div>
            <div class="text">
                According to the National Associaltion of Unclaimed Property Administrators (NAUPA), one out of eight people in the U.S. have unclaimed assets...with average claims of $1,000!  If you have a 1 in 8 chance of getting some free bucks, isn't that worth exploring?
            </div>
            <div class="scale">
                <img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/scale.png" /></div>
            <div class="catbtn">
                <div class="arrow">
                    <img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/.png" /></div>
                <div class="btn">
                    <a href="javascript:void(0);" onclick="togglecat();" /><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/mobilebtn.png" /></a></div>
            </div>
            <div style="clear:both;"></div>
                <div class="greentext">Explore the HomeMoneyGuide                    Categories to discover your Fortune, Lucky Numbers, 
                    Home Money News and more!</div>
        </div>
        <div class="ads_padding_top">
            
        </div>
        <div class="sitemap">
            <asp:Literal runat="server" ID="litCategories"></asp:Literal>
        </div>
    </div>
    <div style="clear:both;"></div>
    <div id="bottom">
        <div class="top">
            <div class="title">The Home Money Guide is your free source for information and updates about:</div>
            <div class="bullets">
                Celebrities <span>•</span> Daily Horoscopes <span>•</span> Lucky Numbers <span>•</span> Featured Articles <span>•</span> Zodiac News <span>•</span> & More
            </div>
        </div>
        <div class="bottom">
            <div class="disclosure">
                ThePersonalFinancialGuide™ is not affiliated with any of the listed products, brands, or organizations, nor do any of them endorse or sponsor ThePersonalFinancialGuide™.<br />The information and astrological interpretations at this site are for entertainment purposes only.
            </div>
            <div class="copy">Copyright &copy; <%=DateTime.Now.Year %> C4R Media Corp.<br />
                ThePersonalFinancialGuide&#8482;
                <a href="/" target="_blank">Home</a> | <a href="/Terms.aspx" target="_blank">Terms and Conditions</a> | <a href="/Privacy.aspx" target="_blank">Privacy Policy</a> | <a href="/Unsub.aspx" target="_blank">Unsubscribe</a> | <a href="/Sitemap" target="_blank">Site Map</a>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
