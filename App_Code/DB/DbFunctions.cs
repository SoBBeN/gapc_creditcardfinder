﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Data.SqlClient;

namespace DB
{
    public class DbFunctions
    {

        private static int WebSiteID
        {
            get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["websiteid"]); }
        }

        public static SqlDataReader GetWebSite()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Website_GetOne] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static void AddiContactUnsub(string email)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[InsertNewUnsub] ");

            SqlConnection conection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Click4RichesConn"].ConnectionString);
            conection.Open();

            query.Append(objSqlServer.FormatSql(112)); // CI CampaignID on C4R
            query.Append(",");
            query.Append(objSqlServer.FormatSql(email));

            try
            {
                objSqlServer.ExecNonQuery(query.ToString(), conection);
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("AddiContactUnsub", ex, "Query: EXEC " + query.ToString());
            }
            finally
            {
                conection.Dispose();
            }
        }

        public static SqlDataReader GetRecipe(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_GetOne] ").Append(objSqlServer.FormatSql(id));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetContent(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Content_GetOne] ").Append(objSqlServer.FormatSql(id));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetContentCategories()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Content_GetAllCategories] ");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetContentCategoriesActive()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Content_GetAllCategoriesActive] ");
            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetAnswers()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Question_GetAnswers] ").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetRecipeCategories()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_GetAllCategories] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetRecipeCategories(int recipeid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_GetAllRecipeCategories] ");

            query.Append(objSqlServer.FormatSql(recipeid));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetRecipeByCategory(string category, int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Get_ByCategory] ");

            query.Append(objSqlServer.FormatSql(category)).Append(",");
            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetRecipeRelated(int recipeid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Get_Related] ");

            query.Append(objSqlServer.FormatSql(recipeid)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }


        public static SqlDataReader GetProduct(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_GetOne] ").Append(objSqlServer.FormatSql(id));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyleWithSlides(int id, string category)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleWithSlides] ").Append(objSqlServer.FormatSql(id));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            query.Append(",").Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);
            query.Append(",").Append(objSqlServer.FormatSql(category));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetProducts()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_Get] 0,0,");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static DataTable GetProductsDt()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_Get] 0,0,");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataTable(query.ToString());
        }

        public static SqlDataReader GetProductCategories()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_GetCategories] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetProductCategories(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_GetProductCategories] ");

            query.Append(objSqlServer.FormatSql(id));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetLifestyleTags(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_GetLifestyleTags] ");

            query.Append(objSqlServer.FormatSql(id));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetProductByCategory(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_Get_ByCategory] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetProductRelated(int Productid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_Get_Related] ");

            query.Append(objSqlServer.FormatSql(Productid)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetProductCategory()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[ProductCategory_Get] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetProductPrevNext(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Product_GetPrevNext] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static void InsertPost(int leadId, short partnerId, string postString, bool isSuccess, string partnerResponse, string values, decimal payed)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[proc_Partners_Insert_Post] ");

            SqlConnection conection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Click4RichesConn"].ConnectionString);
            conection.Open();

            query.Append(objSqlServer.FormatSql(leadId)).Append(",");
            query.Append(objSqlServer.FormatSql(partnerId)).Append(",");
            query.Append(objSqlServer.FormatSql(postString)).Append(",");
            query.Append(objSqlServer.FormatSql(isSuccess)).Append(",");
            query.Append(objSqlServer.FormatSql(partnerResponse)).Append(",");
            query.Append(objSqlServer.FormatSql(payed)).Append(",");
            query.Append(objSqlServer.FormatSql(values));

            try
            {
                objSqlServer.ExecNonQuery(query.ToString(), conection);
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("InsertPost", ex, "Query: EXEC " + query.ToString());
            }
            finally
            {
                conection.Dispose();
            }
        }
        public static bool CheckEmailDup(string email)
        {
            return CheckEmailDup(email, -1);
        }
        public static bool CheckEmailDup(string email, int userid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_CheckEmailDup] ");

            query.Append(objSqlServer.FormatSql(email)).Append(",");
            query.Append(objSqlServer.FormatSql(userid));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString())) > 0;
        }
        public static SqlDataReader CheckEmailStatus(string email, int userid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_CheckEmailDup] ");

            query.Append(objSqlServer.FormatSql(email)).Append(",");
            query.Append(objSqlServer.FormatSql(userid));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static int InsertUser(string firstname, string lastname, string email, string zip, string password, bool isactive, bool ckbAgree, string country)
        {
            return InsertUser(firstname, lastname, email, zip, password, isactive, null, null, ckbAgree, country, int.MinValue);
        }
        public static int InsertUser(string firstname, string lastname, string email, string zip, string password, bool isactive, bool ckbAgree, string country, long facebookid)
        {
            return InsertUser(firstname, lastname, email, zip, password, isactive, null, null, null, null, null, ckbAgree, country, facebookid);
        }
        public static int InsertUser(string firstname, string lastname, string email, string zip, string password, bool isactive, string phone, string affid, bool ckbAgree, string country, long facebookid)
        {
            return InsertUser(firstname, lastname, email, zip, password, isactive, phone, affid, null, null, null, ckbAgree, country, facebookid);
        }
        public static int InsertUser(string firstname, string lastname, string email, string zip, string password, bool isactive, string phone, string affid, string address, string city, string state, bool ckbAgree, string country, long facebookid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_Insert] ");

            query.Append(objSqlServer.FormatSql(firstname)).Append(",");
            query.Append(objSqlServer.FormatSql(lastname)).Append(",");
            query.Append(objSqlServer.FormatSql(email)).Append(",");
            query.Append(objSqlServer.FormatSql(zip)).Append(",");
            if (password != null)
                query.Append(objSqlServer.FormatSql(Functions.GetSha256(password))).Append(",");
            else
                query.Append("null,");
            query.Append(objSqlServer.FormatSql(isactive)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(phone)).Append(",");
            query.Append(objSqlServer.FormatSql(affid)).Append(",");
            query.Append(objSqlServer.FormatSql(address)).Append(",");
            query.Append(objSqlServer.FormatSql(city)).Append(",");
            query.Append(objSqlServer.FormatSql(state)).Append(",");
            query.Append(objSqlServer.FormatSql(ckbAgree)).Append(",");
            query.Append(objSqlServer.FormatSql(country)).Append(",");
            query.Append(objSqlServer.FormatSql(UserSession.GetInstance().SubID)).Append(",");
            query.Append(objSqlServer.FormatSql(facebookid));

            return Convert.ToInt32(objSqlServer.GetFirstRow(query.ToString())["UserID"]);
        }
        public static int InsertUserLivePost(string firstname, string lastname, string email, string zip, string password, bool isactive, string phone, string affid, string address, string city, string state, bool ckbAgree, string country, string dob)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_Insert] ");

            query.Append(objSqlServer.FormatSql(firstname)).Append(",");
            query.Append(objSqlServer.FormatSql(lastname)).Append(",");
            query.Append(objSqlServer.FormatSql(email)).Append(",");
            query.Append(objSqlServer.FormatSql(zip)).Append(",");
            if (password != null)
                query.Append(objSqlServer.FormatSql(Functions.GetSha256(password))).Append(",");
            else
                query.Append("null,");
            query.Append(objSqlServer.FormatSql(isactive)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(phone)).Append(",");
            query.Append(objSqlServer.FormatSql(affid)).Append(",");
            query.Append(objSqlServer.FormatSql(address)).Append(",");
            query.Append(objSqlServer.FormatSql(city)).Append(",");
            query.Append(objSqlServer.FormatSql(state)).Append(",");
            query.Append(objSqlServer.FormatSql(ckbAgree)).Append(",");
            query.Append(objSqlServer.FormatSql(country)).Append(",NULL,NULL,");
            query.Append(objSqlServer.FormatSql(dob));

            return Convert.ToInt32(objSqlServer.GetFirstRow(query.ToString())["EmailListID"]);
        }
        public static void UpdateUser(int userid, string firstname, string lastname, string email, string zip)
        {
            UpdateUser(userid, firstname, lastname, email, zip, null);
        }
        public static void UpdateUser(int userid, string firstname, string lastname, string email, string zip, string phone)
        {
            UpdateUser(userid, firstname, lastname, email, zip, phone);
        }
        public static void UpdateUser(int userid, string firstname, string lastname, string email, string zip, string phone, string address, string city, string state)
        {
            UpdateUser(userid, firstname, lastname, email, zip, phone, address, city, state, null, long.MinValue);
        }
        public static void UpdateUser(int userid, string firstname, string lastname, string email, string zip, string phone, string address, string city, string state, string password, long facebookid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_Update] ");

            query.Append(objSqlServer.FormatSql(userid)).Append(",");
            query.Append(objSqlServer.FormatSql(firstname)).Append(",");
            query.Append(objSqlServer.FormatSql(lastname)).Append(",");
            query.Append(objSqlServer.FormatSql(email)).Append(",");
            query.Append(objSqlServer.FormatSql(zip)).Append(",");
            if (String.IsNullOrEmpty(password))
                query.Append("null,");
            else
                query.Append(objSqlServer.FormatSql(Functions.GetSha256(password))).Append(",");
            query.Append("null,");
            query.Append(objSqlServer.FormatSql(phone)).Append(",");
            query.Append(objSqlServer.FormatSql(address)).Append(",");
            query.Append(objSqlServer.FormatSql(city)).Append(",");
            query.Append(objSqlServer.FormatSql(state)).Append(",");
            query.Append(objSqlServer.FormatSql(facebookid));

            objSqlServer.GetDataReader(query.ToString());
        }
        public static void UpdateUserProfileImage(int userid, string imagefilename)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_Image_Update] ");

            query.Append(objSqlServer.FormatSql(userid)).Append(",");
            query.Append(objSqlServer.FormatSql(imagefilename));

            objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetUser(int userid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_GetOne] ");

            query.Append(objSqlServer.FormatSql(userid));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetUser(string guid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_GetOne_GUID] ");

            query.Append(objSqlServer.FormatSql(guid));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetUserSubmit(int userid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_GetSubmit] ");

            query.Append(objSqlServer.FormatSql(userid));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static DataRow LoginUser(string email, string password)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_Login] ");

            query.Append(objSqlServer.FormatSql(email)).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(password)));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));


            //HttpContext.Current.Response.Write(query.ToString() + password);

            return objSqlServer.GetFirstRow(query.ToString());
        }
        public static DataRow LoginUser(int facebookid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_Login] ");

            query.Append("NULL,NULL,");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(facebookid));


            //HttpContext.Current.Response.Write(query.ToString() + password);

            return objSqlServer.GetFirstRow(query.ToString());
        }
        public static bool ResetPassword(string email, string password)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_ResetPassword] ");

            query.Append(objSqlServer.FormatSql(email)).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(password)));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString())) > 0;
        }
        public static bool ChangePassword(int userid, string newpassword, string oldpassword)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[GAPC].dbo.[BF_User_ChangePassword] ");

            query.Append(objSqlServer.FormatSql(userid)).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(newpassword))).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(oldpassword)));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString())) > 0;
        }


        public static SqlDataReader GetAd(short width, short height)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Ads_Get] ");

            query.Append(objSqlServer.FormatSql(UserSession.GetInstance().SessionID)).Append(",");
            query.Append(objSqlServer.FormatSql(width)).Append(",");
            query.Append(objSqlServer.FormatSql(height));

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static void InsertClickAd(int adimpressionid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Ads_Click_Add] ");

            query.Append(objSqlServer.FormatSql(adimpressionid));

            objSqlServer.ExecNonQuery(query.ToString());
        }

        public static SqlDataReader GetSlider()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Slider_Get] ");

            query.Append("0,").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetCarousel()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Carousel_Get] ");

            query.Append("0,").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetTodaysMom()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Get] 0,").Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetTodaysMomImages(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Image_Get] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetTodaysMom(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetTodaysMom(int id, short diff)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_GetOne] ").Append(id).Append(",").Append(diff);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetTodaysMomPaging(int id, short diff)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Get_Paging] ").Append(id).Append(",").Append(diff);

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static DataTable GetTodaysMomPagingDt(int id, short diff)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_TodaysMom_Get_Paging] ").Append(id).Append(",").Append(diff);

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataTable(query.ToString());
        }
        public static SqlDataReader GetFbDay()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_FbDaySlider_Get] 0,").Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static DataTable GetFbDayDt()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_FbDaySlider_Get] 0,").Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataTable(query.ToString());
        }
        public static SqlDataReader GetFbDay(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_FbDaySlider_GetOne] ").Append(id);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetFbDay(int id, short diff)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_FbDaySlider_GetOne] ").Append(id).Append(",").Append(diff);
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetRssFeeds(int type)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[RssFeeds_GetFeeds] ").Append(type);

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetRssItems(int rssid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[RssFeeds_GetItems] ").Append(rssid);

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetRssItem(int itemid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[RssFeeds_GetItem] ").Append(itemid);

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetEditorPicks()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[EditorPicks_Get]");

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetImagePrevNext(int typeid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Get_PrevNext] ").Append(objSqlServer.FormatSql(typeid));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetImages(short type)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Get] 0,").Append(objSqlServer.FormatSql(type));

            query.Append(",NULL,").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }

        public static DataTable GetImagesDt(short type)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Get] 0,").Append(objSqlServer.FormatSql(type));

            query.Append(",NULL,").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataTable(query.ToString());
        }

        public static DataTable GetImagesRecent()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Get_Recent]");

            query.Append("0,").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataTable(query.ToString());
        }
        public static SqlDataReader GetImagesRecentDr()
        {
            return GetImagesRecentDr(false);
        }
        public static SqlDataReader GetImagesRecentDr(bool ismoments)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Get_Recent] ").Append(ismoments);

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetImagesRecentDrOrderByType(bool ismoments)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Get_Recent_Order] ").Append(ismoments);

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetImagesRecentHomePage(bool ismoments)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Get_Recent_Homepage] ").Append(ismoments);

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetImageTypes()
        {
            StringBuilder query = new StringBuilder("[BF_Image_Types_Get] 1");
            SqlServer objSqlServer = new SqlServer();

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static int InsertImage(short type, string imageFilename, int isActive, string text, string thumbnail, int userid)
        {
            string activeDate = null;
            string title = Functions.RemoveHtml(text);
            string overrideUsername = null;

            StringBuilder query = new StringBuilder("[BF_Image_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(type)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(overrideUsername)).Append(",");
            query.Append(objSqlServer.FormatSql(userid));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }

        public static DataTable GetDaycares()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Daycare_Get]");

            return objSqlServer.GetDataTable(query.ToString());
        }

        public static SqlDataReader GetTalkingPoint()
        {
            return GetTalkingPoint(int.MinValue);
        }
        public static SqlDataReader GetTalkingPoint(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[TalkingPoints_Get] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetArticle()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Articles_Get] ").Append("NULL,0,");
            query.Append(objSqlServer.FormatSql(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSearchResult(String SearchKeyWord)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Search] ").Append(SearchKeyWord);
            //query.Append(objSqlServer.FormatSql(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetArticle(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Articles_Get] ").Append(objSqlServer.FormatSql(id)).Append(",1,");

            query.Append(objSqlServer.FormatSql(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetArticle(string category)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Articles_Get_ByCategory] ");

            query.Append(objSqlServer.FormatSql(category)).Append(",");
            query.Append(objSqlServer.FormatSql(1)).Append(",");
            query.Append(objSqlServer.FormatSql(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetArticle(int id, short diff, string category)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Articles_Get_Paging] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(diff)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        /* public static int GetUserID(FacebookUser fbu)
         {
             SqlServer objSqlServer = new SqlServer();
             StringBuilder query = new StringBuilder("[BF_Users_GetUserID] ");

             string gender;
             if (fbu.gender != null && fbu.gender.Length > 0)
                 gender = fbu.gender.Substring(0, 1);
             else
                 gender = null;

             query.Append(objSqlServer.FormatSql(fbu.id)).Append(",");
             query.Append(objSqlServer.FormatSql(fbu.first_name)).Append(",");
             query.Append(objSqlServer.FormatSql(fbu.last_name)).Append(",");
             query.Append(objSqlServer.FormatSql(fbu.username)).Append(",");
             query.Append(objSqlServer.FormatSql(gender));

             return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
         }*/
        //public static int InsertNewsLetterEmail(string email)
        //{
        //    SqlServer objSqlServer = new SqlServer();
        //    StringBuilder query = new StringBuilder("[Newsletter_Insert] ").Append(objSqlServer.FormatSql(email));

        //    query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
        //    return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        //}



        public static SqlDataReader GetBlogger(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Bloggers_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllBloggers()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Bloggers_Get] 0,").Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllLifestyleCategories()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleCategories_GetActive] ").Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetBloggerBlogs(int bloggerid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Bloggers_Blogs_Get] ").Append(objSqlServer.FormatSql(bloggerid));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static DataTable GetBloggerBlogsDt(int bloggerid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Bloggers_Blogs_Get] ").Append(objSqlServer.FormatSql(bloggerid));

            return objSqlServer.GetDataTable(query.ToString());
        }
        public static DataTable GetBloggerBlogsOneEachDt()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Bloggers_Blogs_GetOneEach] ").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataTable(query.ToString());
        }

        public static SqlDataReader GetBlog(int blogid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Blogs_Get] ").Append(objSqlServer.FormatSql(blogid));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetBlogs()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Blogs_GetAll] 0,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetBlog(int id, short diff, int author)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Blogs_Get] ").Append(id).Append(",").Append(diff).Append(",").Append(author);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetPhotosPaging(int typeid, int id, short diff)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_Get_Paging] ").Append(typeid).Append(",").Append(id).Append(",").Append(diff);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetPhoto(int id, short diff, int typedesc)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Image_GetOne] ").Append(id).Append(",").Append(diff).Append(",").Append(typedesc);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetRealDeals(int category)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_RealDeals_Get] ");

            query.Append(objSqlServer.FormatSql(0)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetRealDealsTypes(bool imageonly)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_RealDeals_Types_Get] ");
            query.Append(objSqlServer.FormatSql(imageonly));

            return objSqlServer.GetDataReader(query.ToString());
        }


        public static SqlDataReader RewardAndGetLink(int userid, int offerid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsRewarded_RewardAndGetLink] ");

            query.Append(objSqlServer.FormatSql(userid)).Append(",");
            query.Append(objSqlServer.FormatSql(offerid));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetPointsOffersUserHistory(int userid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_User_Get] ");

            query.Append(objSqlServer.FormatSql(userid));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetPointsOffersCategories(int pointsOffersid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_Category_Get] ");
            query.Append(pointsOffersid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetPointsOffers(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_GetOne] ");
            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetPointsOffersList(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_GetList] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetNewsPrevNext(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[News_GetPrevNext] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetNews(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[News_GetList] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetNewsOne(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[News_GetOne] ");

            query.Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyle(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_GetList] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyleRandom()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_GetRandom] ");

            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyleTag(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleTag_GetList] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static DataTable GetPointsOffersList()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_GetList] ");

            query.Append("-1,");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataTable(query.ToString());
        }
        public static SqlDataReader GetPointsOffersPrevNext(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[PointsOffers_GetPrevNext] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetLeadData(int leadId)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Partners_GetLeadData2] ");

            SqlConnection conection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Click4RichesConn"].ConnectionString);
            conection.Open();

            query.Append(objSqlServer.FormatSql(leadId));

            return objSqlServer.GetDataReader(query.ToString(), conection);
        }

        public static void PostToAllInbox(string sourceid, string recordid, string email, string fname, string IpAddress, string lname, string phone, string subid)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("resource=contacts&api_key=0da2d62b_fdc3_4377_b8a2_9ac0ffb022f0&source_id=").Append(sourceid);
            sb.Append("&source_sub_id=").Append(HttpUtility.UrlEncode(subid));
            sb.Append("&email=");
            if (email.Contains("@"))
                sb.Append(HttpUtility.UrlEncode(email));
            else
                sb.Append(email);
            sb.Append("&first_name=").Append(HttpUtility.UrlEncode(fname));
            sb.Append("&last_name=").Append(HttpUtility.UrlEncode(lname));
            sb.Append("&ip_address=").Append(IpAddress);
            sb.Append("&phone=").Append(phone);
            sb.Append("&unique_id=").Append(recordid);

            string myParameters = sb.ToString();
            string URI3 = "https://api2.all-inbox.com";

            int recordIdInt;
            if (!int.TryParse(recordid, out recordIdInt))
                recordIdInt = 0;

            try
            {
                InsertPostQueue(recordIdInt, Convert.ToInt16(2), URI3, myParameters, Convert.ToDecimal(0));
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("Partners InsertPostQueue AllInbox", ex, String.Format("PartnerID: 2\nRecordID: {0}\nPostString: {1}\nPostedValues: {2}\n", recordIdInt, URI3, myParameters));
            }
        }

        public static void InsertPostQueue(int leadId, short partnerId, string postString, string values, decimal payed)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder sb = new StringBuilder();

            SqlConnection conection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Click4RichesConn"].ConnectionString);
            conection.Open();

            sb.Append("Partners_InsertQueue ");
            sb.Append(objSqlServer.FormatSql(leadId));
            sb.Append(",");
            sb.Append(objSqlServer.FormatSql(partnerId));
            sb.Append(",");
            sb.Append(objSqlServer.FormatSql(postString));
            sb.Append(",");
            sb.Append(objSqlServer.FormatSql(payed));
            sb.Append(",");
            sb.Append(objSqlServer.FormatSql(values));

            try
            {
                objSqlServer.ExecNonQuery(sb.ToString(), conection);
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("InsertPostQueue", ex, "Query: EXEC " + sb.ToString());
            }
            finally
            {
                conection.Dispose();
            }
        }

        public static SqlDataReader GetSettlementsRebatesList(int id, int order)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SettlementsRebates_GetList] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]).Append(",");
            query.Append(objSqlServer.FormatSql(order));

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader GetSettlementsRebates(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SettlementsRebates_GetOne] ").Append(id);
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSettlementsPrevNext(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Settlements_GetPrevNext] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static DataRow Quote_GetOneRandom(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Quote_GetOneRandom] ");
            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetFirstRow(query.ToString());
        }

        public static DataTable Content_GetCategories(int websiteid)
        {
            DataTable dr = null;

            SqlConnection conection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Click4RichesConn"].ConnectionString);
            conection.Open();

            try
            {
                SqlServer objSqlServer = new SqlServer();
                StringBuilder sb = new StringBuilder();
                sb.Append("EXEC Content_GetCategories ");
                sb.Append(objSqlServer.FormatSql(websiteid));

                dr = objSqlServer.GetDataTable(sb.ToString(), conection);
            }
            catch (Exception ex) { }
            finally
            {
                conection.Dispose();
            }

            return dr;
        }

        public static DataTable Content_GetContents(int categoryid)
        {
            DataTable dr = null;

            SqlConnection conection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Click4RichesConn"].ConnectionString);
            conection.Open();

            try
            {
                SqlServer objSqlServer = new SqlServer();
                StringBuilder sb = new StringBuilder();
                sb.Append("EXEC Content_GetSections ");
                sb.Append(objSqlServer.FormatSql(categoryid));

                dr = objSqlServer.GetDataTable(sb.ToString(), conection);
            }
            catch (Exception ex) { }
            finally
            {
                conection.Dispose();
            }

            return dr;
        }

        public static SqlDataReader GetInternalAdsActive()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[InternalAds_GetAllActive] ");
            query.Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }

        public static SqlDataReader InternalAdsTrackingInsert(int adID, string pageURL, string userAgent, string ipAddress)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[InternalAdsTracking_Insert] ");
            query.Append(objSqlServer.FormatSql(adID)).Append(",");
            query.Append(objSqlServer.FormatSql(pageURL)).Append(",");
            query.Append(objSqlServer.FormatSql(userAgent)).Append(",");
            query.Append(objSqlServer.FormatSql(ipAddress));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSweepContestsCategories(int pointsOffersid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Category_Get] ");
            query.Append(pointsOffersid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSweepContests(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_GetOne] ");
            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSweepContestsList(int id, bool isSample = false)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_GetList] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]).Append(",");
            query.Append(objSqlServer.FormatSql(isSample));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSweepContestsPrevNext(int id, bool isSample = false)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_GetPrevNext] ");

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]).Append(",");
            query.Append(objSqlServer.FormatSql(isSample));

            return objSqlServer.GetDataReader(query.ToString());
        }
    }
}