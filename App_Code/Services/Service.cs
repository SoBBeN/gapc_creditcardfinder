﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Text.RegularExpressions;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class Service : IService
{

	public string GetData(int value)
	{
		return string.Format("You entered: {0}", value);
	}

    public CompositeType GetDataUsingDataContract(CompositeType composite)
    {
        if (composite == null)
        {
            throw new ArgumentNullException("composite");
        }
        if (composite.BoolValue)
        {
            composite.StringValue += "Suffix";
        }
        return composite;
    }

    public void InsertNewsLetterEmail(string email)
    {
        int id = -1;

        try
        {
            if (Regex.IsMatch(email, "^[\\.a-zA-Z\\d_\\-]+[\\@][a-zA-Z\\d\\.\\-]+[\\.][\\.a-zA-Z]+$"))
            {
                id = DB.DbFunctions.InsertUser(null, null, email, null, null, true, false, "US");
            }
            else
            {
                ErrorHandling.SendException("InsertNewsLetterEmail", new Exception("Invalid Email:" + email));
            }
        }
        catch (Exception ex)
        {
            ErrorHandling.SendException("InsertNewsLetterEmail", ex);
        }

        // Post to KOBE
        OperationContext context = OperationContext.Current;
        MessageProperties prop = context.IncomingMessageProperties;
        RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
        string ip = endpoint.Address;
        //Functions.PostToKobe("14292", "1985", email, String.Empty, String.Empty, string.Empty, String.Empty, ip, String.Empty, id.ToString());

        //Functions.PostToAllInbox(
    }

    public FbPageDaySerializable GetFbPageDay(int id, short diff)
    {
        FbPageDaySerializable fb = new FbPageDaySerializable();
        SqlDataReader dr = DB.DbFunctions.GetFbDay(id, diff);

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                fb.ID = Convert.ToInt32(dr["ID"]);
                fb.Image = "facebook/" + Convert.ToString(dr["ImageFilename"]);
                fb.Text = Convert.ToString(dr["FullText"]);
                fb.Link = "/fbpage/" + fb.ID.ToString() + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                fb.Title = Convert.ToString(dr["Title"]);
                fb.LinkOutside = Convert.ToString(dr["LinkURL"]);
                fb.Day = Functions.UppercaseFirst(Convert.ToDateTime(dr["DateIN"]).ToString("MMM d, yyyy"));
            }
        }
        dr.Close();
        return fb;
    }

    public TodaysMomSerializable GetTodaysMom(int id, short diff)
    {
        TodaysMomSerializable tm = new TodaysMomSerializable();
        SqlDataReader dr = DB.DbFunctions.GetTodaysMom(id, diff);

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                tm.ID = Convert.ToInt32(dr["ID"]);
                tm.Image = "todaysmom/" + Convert.ToString(dr["ImageFilename"]);
                tm.Text = Convert.ToString(dr["Text"]);
                tm.Link = "/todaysmom/" + tm.ID.ToString() + "/" + Functions.StrToURL(Convert.ToString(dr["Name"])) + "/";
                tm.Title = Convert.ToString(dr["Name"]);
                tm.Description = Convert.ToString(dr["Occupation"]);
                tm.Day = Functions.UppercaseFirst(Convert.ToDateTime(dr["DateIN"]).ToString("MMM d, yyyy"));
                tm.MomLink = Convert.ToString(dr["LinkURL"]);
                tm.LinkTitle = Convert.ToString(dr["LinkTitle"]);
                tm.LinkLogo = "todaysmom/" + Convert.ToString(dr["LinkLogo"]);
                tm.Facebook = Convert.ToString(dr["Facebook"]);
                tm.Twitter = Convert.ToString(dr["Twitter"]);
                tm.Pinterest = Convert.ToString(dr["Pinterest"]);
                if (dr.NextResult())
                {
                    if (dr.Read())
                        tm.Image1 = "todaysmom/" + Convert.ToString(dr["ImageFilename"]);
                    if (dr.Read())
                        tm.Image2 = "todaysmom/" + Convert.ToString(dr["ImageFilename"]);
                    if (dr.Read())
                        tm.Image3 = "todaysmom/" + Convert.ToString(dr["ImageFilename"]);
                }
            }
        }
        dr.Close();
        return tm;
    }

    public BlogSerializable GetBlog(int id, short diff, int author)
    {
        BlogSerializable bs = new BlogSerializable();
        SqlDataReader dr = DB.DbFunctions.GetBlog(id, diff, author);

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                bs.ID = Convert.ToInt32(dr["ID"]);
                bs.Image = "blogs/" + Convert.ToString(dr["ImageFilename"]);
                bs.Text = Convert.ToString(dr["Text"]);
                bs.Title = Convert.ToString(dr["Title"]);
                bs.Day = Functions.UppercaseFirst(Convert.ToDateTime(dr["DateIN"]).ToString("MMM d, yyyy"));
                bs.Link = "/Blogs/" + bs.ID.ToString() + "/" + Functions.StrToURL(Convert.ToString(dr["Firstname"]) + " " + Convert.ToString(dr["Lastname"])) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
            }
        }
        dr.Close();
        return bs;
    }

    public PhotoSerializable GetPhoto(int id, short diff, int typeid)
    {
        PhotoSerializable s = new PhotoSerializable();
        SqlDataReader dr = DB.DbFunctions.GetPhoto(id, diff, typeid);

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                s.ID = Convert.ToInt32(dr["ID"]);
                s.Image = "images/" + Convert.ToString(dr["ImageFilename"]);
                s.Type = Convert.ToInt32(dr["Type"]);
                s.Title = Functions.RemoveHtml(Convert.ToString(dr["Text"]));
                s.Text = Convert.ToString(dr["Text"]);
                s.TypeDesc = Convert.ToString(dr["TypeDesc"]);
                //s.Link = "/Photos/" + s.ID.ToString() + "/" + Functions.StrToURL(s.TypeDesc) + "/" + s.Type.ToString() + "/";
                s.Link = Functions.StrToURL(s.TypeDesc);
            }
        }
        dr.Close();
        return s;
    }

    public string GetTodaysMoms(int id, short diff)
    {
        StringBuilder sb = new StringBuilder();

        SqlDataReader dr = DB.DbFunctions.GetTodaysMomPaging(id, diff);

        if (dr != null && dr.HasRows)
        {
            int i = 0;

            while (dr.Read() && i++ < 9)
            {
                string link = String.Format("<a href=\"/TodaysMom/{0}/{1}/\"", dr["id"], Functions.StrToURL(Convert.ToString(dr["Name"])));

                if (i == 1)
                    sb.Append(link).Append("<input type=\"hidden\" id=\"firstid\" value=\"").Append(dr["ID"]).Append("\" />");

                sb.Append("<div class=\"mom\"");
                sb.Append(">").Append(link).Append("><img src=\"//www.lovehopeluck.com/images/todaysmom/").Append(dr["ImageFilename"]).Append("\" />");
                sb.Append(link).Append("><span class=\"name\">").Append(dr["Name"]).Append("</span></a>");
                //sb.Append("<span class=\"location\">").Append(dr["Location"]).Append("</span>");
                sb.Append("<span class=\"occupation\">").Append(dr["Occupation"]).Append("</span>");
                sb.Append(link).Append(">Read More...</a></div>");
            }
            sb.Append("<div style=\"clear:both;\"></div>");
        }
        dr.Close();
        return sb.ToString();
    }

    public string GetPhotos(int id, short diff, int type)
    {
        StringBuilder sb = new StringBuilder();

        SqlDataReader dr = DB.DbFunctions.GetPhotosPaging(type, id, diff);

        if (dr != null && dr.HasRows)
        {
            int i = 0;
            bool adShown = false;

            while (dr.Read() && i++ < 9)
            {
                if (i == 1)
                    sb.Append("<input type=\"hidden\" id=\"firstid\" value=\"").Append(dr["ID"]).Append("\" />");

                string link = String.Format("<a href=\"javascript:void(0);\" onclick=\"OpenPhoto({0});\"", Convert.ToString(dr["ID"]));

                sb.Append("<div class=\"photo\">");
                sb.Append("<div onclick=\"OpenPhoto(").Append(dr["ID"]).Append(")\" style=\"background-image:url('" + System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "images/").Append(Convert.ToString(dr["ImageFilename"]).Replace("'", "\\'")).Append("');\" /></div>\n");
                sb.Append("</div>");

                if (i % 3 == 0)
                {
                    if (HttpContext.Current.Request.Browser.IsMobileDevice && !adShown)
                    {
                        sb.Append("<div class=\"ad\">");
                        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                        sb.Append("</div><div style=\"clear:both;\">");

                        adShown = true;
                    }
                }
            }

            if (i < 3)
            {
                if (HttpContext.Current.Request.Browser.IsMobileDevice)
                {
                    sb.Append("<div class=\"ad\">");
                    sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                    sb.Append("</div><div style=\"clear:both;\"></div>");
                }
            }
        }
        dr.Close();
        return sb.ToString();
    }

    public string GetMOMents(int photoid, short diff, int typeid)
    {
        StringBuilder sb = new StringBuilder();
        StringBuilder sbScripts = new StringBuilder();

        SqlDataReader dr = DB.DbFunctions.GetPhotosPaging(typeid, photoid, diff);

        if (dr != null && dr.HasRows)
        {
            int i = 0;

            while (dr.Read() && i++ < 9)
            {
                if (i == 1)
                    sb.Append("<input type=\"hidden\" id=\"firstid\" value=\"").Append(dr["ID"]).Append("\" />");

                if (i % 3 == 1)
                {
                    //sb.Append("<div style=\"clear:both;\"></div>\n");
                    sb.Append("<tr>\n");
                }

                sb.Append("<td class=\"photo\">\n");
                sb.Append("<div class=\"top\" style=\"background-image:url('//www.lovehopeluck.com/images/images/").Append(Convert.ToString(dr["ImageFilename"]).Replace("'", "\\'")).Append("');\" /><div onclick=\"OpenPhoto(").Append(dr["ID"]).Append(")\" class=\"over\"></div></div>\n");

                string username;
                if (dr["OverrideUsername"] == DBNull.Value)
                {
                    username = Functions.ConvertToString(dr["Lastname"]);
                    if (username.Length > 1)
                        username = username.Substring(0, 1) + '.';
                    else if (username.Length == 1)
                        username += ".";
                    username = Functions.ConvertToString(dr["Firstname"]) + ' ' + username;
                }
                else
                {
                    username = Functions.ConvertToString(dr["OverrideUsername"]);
                }

                sb.Append("<div class=\"bottom\" onclick=\"OpenPhoto(").Append(dr["ID"]).Append(")\">Shared By <span>").Append(username).Append("</span> on <span>").Append(Convert.ToDateTime(dr["DateIN"]).ToString("MMM d")).Append("</span>\n");

                string imgsrc = "//www.lovehopeluck.com/images/images/" + Convert.ToString(dr["ImageFilename"]);
                int type = Convert.ToInt32(dr["Type"]);
                string titletext = Functions.RemoveHtml(Convert.ToString(dr["Text"]));
                string titleurl = "/Photos/" + type + "/" + Functions.StrToURL(Convert.ToString(dr["TypeDesc"])) + "/" + photoid + "/";

                sb.Append("<div class=\"media\">\n");
                sb.Append("    <div id=\"fb").Append(photoid).Append("\" class=\"social facebook\" onclick=\"gotoFb(\'").Append(titletext).Append("\',\'").Append(imgsrc).Append("\',").Append(typeid).Append(",\'").Append(titleurl).Append("\',").Append(photoid).Append(");\">\n");
                sb.Append("        <div>0</div>\n");
                sb.Append("    </div>\n");
                sb.Append("    <div class=\"social twitter\" onclick=\"gotoTw(\'").Append(titletext).Append("\',\'").Append(typeid).Append("\',\'").Append(titleurl).Append("\',\'").Append(photoid).Append("\');\">\n");
                //sb.Append("        <div>0</div>\n");
                sb.Append("    </div>\n");
                sb.Append("    <div class=\"social pinterest\">\n");
                sb.Append("        <a target=\"_blank\" id=\"tmpinit\" href=\"javascript:void(0)\" data-pin-do=\"buttonPin\" data-pin-config=\"beside\" onclick=\"gotoPin(\'").Append(imgsrc).Append("\',").Append(typeid).Append(",\'").Append(titleurl).Append("\',").Append(photoid).Append(");\"><img src=\"//assets.pinterest.com/images/pidgets/pin_it_button.png\"></a>\n");
                sb.Append("    </div>\n");
                sb.Append("    <div style=\"clear: both;\"></div>\n");
                sb.Append("</div>\n");

                sb.Append("</div></td>\n");

                sbScripts.Append("getfbcount(geturl(").Append(typeid).Append(",\'").Append(titleurl).Append("\',").Append(photoid).Append("), \'#fb").Append(photoid).Append(" div\');\n");


                if (i % 3 == 0)
                {
                    //sb.Append("<div style=\"clear:both;\"></div>\n");
                    sb.Append("</tr>\n");
                }
            }
            if (i % 3 != 0)
            {
                if (i % 3 == 2)
                    sb.Append("<td class=\"photo\"></td>");
                if (i % 3 == 1)
                    sb.Append("<td class=\"photo\"></td><td class=\"photo\"td>");
                //sb.Append("<div style=\"clear:both;\"></div>\n");
                sb.Append("</tr>\n");
            }
        }
        dr.Close();
        return sb.ToString();
    }

    public ArticleSerializable GetArticle(int id, short diff, string category)
    {
        ArticleSerializable obj = new ArticleSerializable();
        SqlDataReader dr = DB.DbFunctions.GetArticle(id, diff, category);

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                obj.ID = Convert.ToInt32(dr["ID"]);

                if (dr["ImageFilename"] != DBNull.Value)
                    obj.Text = "<img id=\"artPhoto\" src=\"//www.lovehopeluck.com/images/articles/" + Convert.ToString(dr["ImageFilename"]) + "\">";
                else
                    obj.Text = String.Empty;
                obj.Text += Convert.ToString(dr["Text"]);

                obj.Title = Convert.ToString(dr["Title"]);
                obj.Link = "/Article/" + id.ToString() + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
            }
        }
        dr.Close();
        return obj;
    }
}
