﻿using System;
using System.Data;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

/// <summary>
/// Summary description for BlogSerializable
/// </summary>
public class PhotoSerializable
{
    #region "Private Members"

    private int id;
    private string image;
    private string typeDesc;
    private int type;
    private string title;
    private string text;
    private string link;

    #endregion

    [DataMember()]
    public int ID
    {
        get { return id; }
        set { id = value; }
    }

    [DataMember()]
    public string Image
    {
        get { return image; }
        set { image = value; }
    }

    [DataMember()]
    public string TypeDesc
    {
        get { return typeDesc; }
        set { typeDesc = value; }
    }

    [DataMember()]
    public int Type
    {
        get { return type; }
        set { type = value; }
    }

    [DataMember()]
    public string Title
    {
        get { return title; }
        set { title = value; }
    }

    [DataMember()]
    public string Text
    {
        get { return text; }
        set { text = value; }
    }

    [DataMember()]
    public string Link
    {
        get { return link; }
        set { link = value; }
    }

	public PhotoSerializable()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}