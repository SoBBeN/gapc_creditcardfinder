﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PointsOffersList.aspx.cs" Inherits="PointsOffersList" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="/css/pointsoffers.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />

    <div id="content">
        <h1><asp:Literal runat="server" ID="litcategory">Find Credit Cards</asp:Literal></h1>
        <hr class="blueline" />
        <div id="coupons">
            <asp:Literal runat="server" ID="litCoupons"></asp:Literal>
        </div>
        <div id="poPrevNext">
            <div class="prev"><a runat="server" id="lnkPrev" class="lnk" visible="false">< previous</a></div>
            <div class="next"><a runat="server" id="lnkNext" class="lnk" visible="false">next ></a></div>
            <div style="clear:both;"></div>
        </div>
    </div>
</asp:Content>