﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SamplesList : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 6;

    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("Samples", "/Samples/");

        SqlDataReader dr;

        dr = DB.DbFunctions.GetSweepContestsList(-1, true);

        if (dr != null)
        {
            if (dr.HasRows)
            {
                int start = 0;
                if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
                {
                    lnkPrev.HRef = "/Samples/?s=" + (start - NB_PER_PAGE).ToString();
                    lnkPrev.Visible = true;
                }

                lnkNext.HRef = "/Samples/?s=" + (start + NB_PER_PAGE).ToString();
                while (start-- > 0 && dr.Read()) ;

                int i = 0;
                StringBuilder sb = new StringBuilder();
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    string link = "<a href=\"/Samples/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "\">";

                    sb.Append("<div class=\"coupon\">");
                    sb.AppendFormat("<div class=\"title\">{0}{1}</a></div>", link, Functions.ConvertToString(dr["Title"]));
                    sb.Append(link).AppendFormat("<div class=\"img\"><img src=\"{0}\" alt=\"Enter {1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagessweepcontesturl"] + Functions.ConvertToString(dr["Thumbnail"]), Functions.ConvertToString(dr["Title"]));
                    sb.Append("<div class=\"details\">");
                    if (dr["DateExpire"] != DBNull.Value)
                    {
                        DateTime expireDate = Convert.ToDateTime(dr["DateExpire"]);
                        sb.Append("<div class=\"expire\"><span style=\"font-style:italic\">Ends on</span> " + Functions.UppercaseFirst(expireDate.ToString("MMMM d")) + Functions.GetDateSuffix(expireDate) + ", " + expireDate.ToString("yyyy") + "</div>");
                    }
                    sb.AppendFormat("<div class=\"description\">{0}</div>", Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 300));
                    sb.AppendFormat("<div class=\"button pink\">{0}<img src=\"/images/signup_button.png\" alt=\"Enter {1}\" /></a></div>", link, Functions.ConvertToString(dr["Title"]));

                    sb.Append("</div>");
                    sb.Append("</div><div style=\"clear:both;\"></div><hr />");

                    //if (i % 3 == 0)
                    //{
                    //    if (Request.Browser.IsMobileDevice)
                    //    {
                    //        sb.Append("<div class=\"ad\">");
                    //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- Free Samples - Bottom --><ins class=\"adsbygoogle\" style=\"display:display:block;width:98%;height:90px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"7335364784\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                    //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                    //    }
                    //}
                    litCoupons.Text = "<hr class=\"blueline\" />" + sb.ToString();
                }

                //if (i < 3)
                //{
                //    if (Request.Browser.IsMobileDevice)
                //    {
                //        sb.Append("<div class=\"ad\">");
                //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- Free Samples - Bottom --><ins class=\"adsbygoogle\" style=\"display:display:block;width:98%;height:90px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"7335364784\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                //    }
                //}

                if (i > NB_PER_PAGE)
                    lnkNext.Visible = true;
            }
            dr.Close();
            dr.Dispose();
        }

        dr = DB.DbFunctions.GetContent(100);

        if (dr != null)
        {
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    litDescriptionTop.Text = Functions.ConvertToString(dr["DescriptionTop"]);
                    litDescriptionBtm.Text = Functions.ConvertToString(dr["DescriptionBtm"]);

                    if (dr["ImageFilename"] != null && Convert.ToString(dr["ImageFilename"]).Length > 0)
                    {
                        if (Functions.ConvertToString(dr["ImageLink"]).Length > 0)
                            litImage.Text = "<a href=\"" + Functions.ConvertToString(dr["ImageLink"]) + "\" target=\"_blank\">";
                        else
                            litImage.Text = "<a>";

                        litImage.Text += "<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["baseimagescontentsurl"] + Functions.ConvertToString(dr["ImageFilename"]) + "\" id=\"imgMain\" class=\"img\" />";
                        litImage.Text += "</a>";
                    }
                    else
                        litImage.Visible = false;
                }
            }

            dr.Close();
            dr.Dispose();
        }
    }
}