﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SettlementsRebates.aspx.cs" Inherits="SettlementsRebates" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="/css/Settlements.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />

    <div id="content">
        <div class="Settlements">
            <h1 class="Settlementtitle">Open Settlements & Rebates</h1>
            <div class="Settlementsort">
                <asp:DropDownList runat="server" ID="ddlSort" onchange="location = this.options[this.selectedIndex].value;">
                    <asp:ListItem Value="">Sort by</asp:ListItem>
                    <asp:ListItem Value="/Rebates?order=1">Deadline</asp:ListItem>
                    <asp:ListItem Value="/Rebates?order=2">Title</asp:ListItem>
                    <asp:ListItem Value="/Rebates?order=3">Date Added</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionTop"></asp:Literal></div>
    <asp:Literal runat="server" ID="litImage"></asp:Literal>
    <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionBtm"></asp:Literal></div>
    <div id="coupons" class="clear">
        <asp:Literal runat="server" ID="litCoupons"></asp:Literal>
    </div>
    <div id="poPrevNext">
        <div class="prev"><a runat="server" id="lnkPrev" class="lnk" visible="false">< previous</a></div>
        <div class="next"><a runat="server" id="lnkNext" class="lnk" visible="false">next ></a></div>
        <div style="clear:both;"></div>
    </div>
    <br /><br />
</asp:Content>
