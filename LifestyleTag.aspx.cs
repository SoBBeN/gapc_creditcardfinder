﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LifestyleTag : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 10;

    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("MoneyTalk", "/Lifestyle/");

        SqlDataReader dr;

        int id;
        if (!int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
        {
            id = int.MinValue;
        }

        dr = DB.DbFunctions.GetLifestyleTag(id);

        if (dr != null)
        {
            string mycategory = Convert.ToString(Page.RouteData.Values["tag"]);
            string mylink = "/LifestyleTag/" + id.ToString() + "/" + mycategory + "/";
            litcategory.Text = mycategory.Replace("_"," ");
            breadcrumbs1.AddLevel(mycategory, mylink);
            ((ITmgMasterPage)Master).PageTitle = mycategory;
            ((ITmgMasterPage)Master).PageURL = mylink;
            ((ITmgMasterPage)Master).PageType = "article";

            if (dr.HasRows)
            {
                int start = 0;
                if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
                {
                    lnkPrev.HRef = mylink + "?s=" + (start - NB_PER_PAGE).ToString();
                    lnkPrev.Visible = true;
                }

                lnkNext.HRef = mylink + "?s=" + (start + NB_PER_PAGE).ToString();
                while (start-- > 0 && dr.Read()) ;

                int i = 0;
                StringBuilder sb = new StringBuilder();
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    string url = "Lifestyle/" + dr["ID"] + "/" + Functions.StrToURL(mycategory) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    string link = String.Format("<a href=\"/{0}\">", url);
                    string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Convert.ToString(dr["Thumbnail"]);
                    string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Description"])), 300).Replace("\"", " ").Trim();
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 300).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"lifestyle\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", title, link);
                    sb.Append(link).AppendFormat("<div class=\"img\"><img src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"],Functions.ConvertToString(dr["Thumbnail"]));
                    sb.AppendFormat("<div class=\"description\">{0}</div>", description);
                    if (dr["DateIN"] != DBNull.Value)
                    {
                        DateTime lifestyleDate = Convert.ToDateTime(dr["DateIN"]);
                        sb.Append("<div class=\"date\"> " + Functions.UppercaseFirst(lifestyleDate.ToString("MMMM d")) + Functions.GetDateSuffix(lifestyleDate) + ", " + lifestyleDate.ToString("yyyy") + "</div>");
                    }

                    sb.Append("</div><div style=\"clear:both;\"></div><hr />");

                    litArticles.Text = "<hr />" + sb.ToString();
                }
                if (i > NB_PER_PAGE)
                    lnkNext.Visible = true;
            }
            dr.Close();
            dr.Dispose();
        }
    }
}