﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


public partial class _Default : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 1;
    protected string dynamicStyles = string.Empty;
    protected string fbTalkingPointLink;
    protected string litFbDayJS;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ((ITmgMasterPage)Master).PageDescription = System.Configuration.ConfigurationManager.AppSettings["description"];
            ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["logo"];

            int id;
            id = -2147483648;

            SqlDataReader dr;
            dr = DB.DbFunctions.GetLifestyle(id);

            if (dr.HasRows)
            {
                dr.NextResult();
                int i = 0;
                StringBuilder sb = new StringBuilder();
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    string url = "Lifestyle/" + dr["ID"] + "/" + Functions.StrToURL(dr["CategoryDescription"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    string link = String.Format("<a href=\"/{0}\">", url);
                    string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Convert.ToString(dr["Thumbnail"]);
                    string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["DescriptionTop"]) + " " + Convert.ToString(dr["Description"])), 150).Replace("\"", " ").Trim();
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 300).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"lifestyle\">");
                    sb.Append("<div class=\"img\">").Append(link).AppendFormat("<img src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"], Functions.ConvertToString(dr["Thumbnail"]));
                    sb.Append("<div class=\"description\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", title, link);
                    sb.Append(description);
                    sb.Append("</div>");
                    sb.Append("</div>");
                }

                //litArticles.Text = sb.ToString();
            }

            id = -1;

            dr = DB.DbFunctions.GetNews(id);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    int i = 0;
                    StringBuilder sb = new StringBuilder();
                    bool first = true;
                    while (dr.Read() && i++ < NB_PER_PAGE)
                    {
                        string newsurl = "/News/" + dr["ID"] + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                        string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"] + Convert.ToString(dr["Thumbnail"]);
                        string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 220).Replace("\"", " ").Trim();
                        string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 220).Replace("\"", " ").Trim();

                        sb.Append("<div class=\"new\">");
                        sb.AppendFormat("<div class=\"img\"><a href=\"{3}\"><img alt=\"{2}\" src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"], Functions.ConvertToString(dr["Thumbnail"]), Convert.ToString(dr["Title"]), newsurl);
                        sb.Append("<div class=\"description\">");
                        sb.AppendFormat("<h2 class=\"title\"><a href=\"{0}\">{1}</a></a></h2>", newsurl, Functions.ConvertToString(dr["Title"]));
                        sb.Append(Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 300));
                        sb.Append("</div>");
                        sb.Append("</div>");

                        litNews.Text = sb.ToString();
                    }
                }
                dr.Close();
                dr.Dispose();
            }

            dr = DB.DbFunctions.GetPointsOffersList(id);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    int i = 0;
                    StringBuilder sb = new StringBuilder();
                    while (dr.Read() && i++ < NB_PER_PAGE)
                    {
                        string link = "<a href=\"/Products/" + dr["ID"] + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Title"])) + "/\">";

                        sb.Append("<div class=\"new\">");
                        sb.AppendFormat("<div class=\"img\">{3}<img alt=\"{2}\" src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"], Functions.ConvertToString(dr["Thumbnail"]), Convert.ToString(dr["Title"]), link);
                        sb.Append("<div class=\"description\">");
                        sb.AppendFormat("<h2 class=\"title\">{0}{1}</a></a></h2>", link, Functions.ConvertToString(dr["Title"]));
                        sb.Append(Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 300));
                        sb.Append("</div>");
                        sb.Append("</div>");
                    }

                    litProducts.Text = sb.ToString();
                }
                dr.Close();
                dr.Dispose();
            }

            dr = DB.DbFunctions.GetContent(277);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        litDescriptionTop.Text = Functions.ConvertToString(dr["DescriptionTop"]);
                        litDescriptionBtm.Text = Functions.ConvertToString(dr["DescriptionBtm"]);

                        if (dr["ImageFilename"] != null && Convert.ToString(dr["ImageFilename"]).Length > 0)
                        {
                            if (Functions.ConvertToString(dr["ImageLink"]).Length > 0)
                                litImage.Text = "<a href=\"" + Functions.ConvertToString(dr["ImageLink"]) + "\" target=\"_blank\">";
                            else
                                litImage.Text = "<a>";

                            litImage.Text += "<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["baseimagescontentsurl"] + Functions.ConvertToString(dr["ImageFilename"]) + "\" id=\"imgMain\" class=\"img\" />";
                            litImage.Text += "</a>";
                        }
                        else
                            litImage.Visible = false;
                    }
                }

                dr.Close();
                dr.Dispose();
            }

            if (Request.QueryString["lid"] != null && Request.QueryString["lid"] != "")
            {
                dr = DB.DbFunctions.GetLeadData(int.Parse(Request.QueryString["lid"].ToString()));

                if (dr != null)
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //var forwardedFor = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                            //string IpAddress = String.IsNullOrWhiteSpace(forwardedFor) ? Request.ServerVariables["REMOTE_ADDR"] : forwardedFor.Split(',').Select(s => s.Trim()).First();
                            string IpAddress = Request.UserHostAddress;
                            DB.DbFunctions.PostToAllInbox("716", dr["SessionID"].ToString(), dr["emailaddress"].ToString(), dr["firstname"].ToString(), IpAddress, dr["lastname"].ToString(), dr["homephoneno"].ToString(), dr["referid"].ToString());
                        }
                    }
                }

                dr.Close();
                dr.Dispose();
            }
        }
    }
}