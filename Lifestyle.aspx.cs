﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Lifestyle : System.Web.UI.Page
{
    protected int nbslide = 0;
    protected int slideshown = 1;
    protected string redirect = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("MoneyTalk", "/Lifestyle/");

        int id;

        if (!int.TryParse(Convert.ToString(Request.QueryString["id"]), out id))
        {
            if (int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
                id = Convert.ToInt32(Page.RouteData.Values["id"]);
            else
                id = -1;
        }

        int selectedSlide;
        if (!int.TryParse(Convert.ToString(Page.RouteData.Values["slideid"]), out selectedSlide))
            selectedSlide = -1;

        string mycategory = Convert.ToString(Page.RouteData.Values["category"]);

        SqlDataReader dr;
        StringBuilder sb;

        if (id > 0)
        {
            dr = DB.DbFunctions.GetLifestyleWithSlides(id, mycategory);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    sb = new StringBuilder();
                    int i = 1;

                    while (dr.Read())
                    {
                        if (i == 1)
                        {
                            ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Functions.ConvertToString(dr["ImageFilename"]);
                            breadcrumbs1.AddLevel(Convert.ToString(dr["Category"]), "/Lifestyle/" + Convert.ToString(dr["CategoryID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/");
                            breadcrumbs1.AddLevel(Convert.ToString(dr["Title"]), "/Lifestyle/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/");
                            //fbComments1.Href = System.Configuration.ConfigurationManager.AppSettings["baseurl"] + "Lifestyle/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                            ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["Title"]);
                            ((ITmgMasterPage)Master).PageURL = "Lifestyle/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                            ((ITmgMasterPage)Master).PageType = "article";
                            ((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["Description"]);

                            litTitle.Text = Functions.ConvertToString(dr["Title"]);
                            litSubTitle.Text = Functions.ConvertToString(dr["SubTitleLifestyle"]);
                            //litDate.Text = Functions.UppercaseFirst(Convert.ToDateTime(dr["DateIN"]).ToString("MMM d, yyyy"));
                        }

                        sb.Append("<div class=\"slide\"");
                        if ((selectedSlide == -1 && i != 1) || (selectedSlide > 0 &&  Convert.ToInt32(dr["SlideID"]) != selectedSlide))
                            sb.Append(" style=\"display:none;\"");
                        else if (selectedSlide > 0 &&  Convert.ToInt32(dr["SlideID"]) == selectedSlide)
                            slideshown = i;
                        sb.AppendFormat(" id=\"slide{1}\"><div class=\"subtitle\">{0}</asp:Literal></div>", dr["SubTitle"], i);
                        sb.AppendFormat("<div class=\"others\">{0}</div>", Convert.ToString(dr["DescriptionTop"]));
                        sb.AppendFormat("<div class=\"video\" align=\"center\">{0}</div>", Convert.ToString(dr["VideoScript"]));
                        if (Convert.ToBoolean(dr["ImageVisible"]))
                        {
                            sb.AppendFormat("<div class=\"imgmain\"><img src=\"{0}\" runat=\"server\" id=\"imgMain\" /><div class=\"leftarrow\"><a href=\"javascript:void(0);\" onclick=\"prevslide();\"><img src=\"/images/arrow_left.png\" /></a></div><div class=\"rightarrow\"><a href=\"javascript:void(0);\" onclick=\"nextslide();\"><img src=\"/images/arrow_right.png\" /></a></div></div>", System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Functions.ConvertToString(dr["ImageFilename"]));
                        }
                        sb.AppendFormat("<div class=\"others\">{0}</div>", Convert.ToString(dr["Description"]));

                        litDots.Text += String.Format(" <a href=\"javascript:void(0);\" onclick=\"changeslide({2});\" id=\"dot{0}\"{1}>&#9679;</a> ", i, ((selectedSlide == -1 && i == 1) || (selectedSlide > 0 && Convert.ToInt32(dr["SlideID"]) == selectedSlide)) ? "class=\"selected\"" : String.Empty, i);

                        sb.Append("</div>");
                        i++;
                    }
                    nbslide = --i;
                    litSlides.Text = sb.ToString();
                }

                dr.NextResult();
                if (dr.Read())
                {
                    if (Convert.ToString(dr["Type"]) == "Category")
                    {
                        redirect = "/Lifestyle/" + dr["ID"] + "/" + Functions.StrToURL(Convert.ToString(dr["Description"])) + "/";
                    }
                    else // Article
                    {
                        redirect = "/Lifestyle/" + dr["ID"] + "/" + Functions.StrToURL(mycategory) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    }
                }
                dr.Close();
                dr.Dispose();
            }

            // Tags (Categories) list
            dr = DB.DbFunctions.GetLifestyleTags(id);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    int groupby = 10;
                    sb = new StringBuilder();
                    while (dr.Read())
                    {
                        if (sb.Length > 0)
                            sb.Append(", ");
                        sb.AppendFormat("\n<a href=\"/LifestyleTag/{0}/{1}/\">{2}</a>", dr["ID"], Functions.StrToURL(dr["Description"]), dr["Description"]);
                    }
                    litTags.Text = sb.ToString();
                }
                dr.Close();
                dr.Dispose();
            }
        }
    }

    protected string FormatTime(object obj)
    {
        string str = String.Empty;
        if (obj != DBNull.Value && obj != null)
        {
            int time;
            if (int.TryParse(Convert.ToString(obj), out time))
            {
                if (time < 60)
                    str = time.ToString() + " mins";
                else
                    str = (time / 60).ToString() + ":" + (time % 60).ToString("X2");
            }
        }
        return str;
    }
}