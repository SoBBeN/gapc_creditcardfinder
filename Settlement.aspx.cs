﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Settlement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("Settlements", "/Rebates/");
        int id;

        if (!int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
        {
            id = int.MinValue;
        }

        SqlDataReader dr = DB.DbFunctions.GetSettlementsRebates(id);

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                id = Convert.ToInt32(dr["ID"]);

                breadcrumbs1.AddLevel(Convert.ToString(dr["Title"]), "Rebates/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/");

                ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimagessettlementsrebatesurl"] + Functions.ConvertToString(dr["Thumbnail"]);
                fbComments1.Href = System.Configuration.ConfigurationManager.AppSettings["baseurl"] + "Rebates/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["Title"]);
                ((ITmgMasterPage)Master).PageURL = "/Rebates/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                ((ITmgMasterPage)Master).PageType = "article";
                ((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["Text"]);

                litTitle.Text = Functions.ConvertToString(dr["Title"]);
                litDate.Text = "Settlement Deadline: " + Functions.UppercaseFirst(Convert.ToDateTime(dr["SettlementDeadline"]).ToString("MMMM d, yyyy"));
                imgTopCoupon.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagessettlementsrebatesurl"] + Functions.ConvertToString(dr["Thumbnail"]);

                litText.Text = Functions.ConvertToString(dr["Text"]);

                if (dr["Eligible"] != null && dr["Eligible"].ToString() != "")
                {
                    var Eligible = "<div class=\"section\"><div class=\"menu\">Who's Eligible</div><div class=\"text\">" + dr["Eligible"] + "</div></div>";
                    litEligible.Text = Eligible;
                }
                if (dr["PotentialAward"] != null && dr["PotentialAward"].ToString() != "")
                {
                    var PotentialAward = "<div class=\"section\"><div class=\"menu\">Potential Award</div><div class=\"text\">" + dr["PotentialAward"] + "</div></div>";
                    litPotentialAward.Text = PotentialAward;
                }
                if (dr["ProofPurchase"] != null && dr["ProofPurchase"].ToString() != "")
                {
                    var ProofPurchase = "<div class=\"section\"><div class=\"menu\">Proof of Purchase</div><div class=\"text\">" + dr["ProofPurchase"] + "</div></div>";
                    litProofPurchase.Text = ProofPurchase;
                }
                if (dr["ClaimFormDeadline"] != null && dr["ClaimFormDeadline"].ToString() != "")
                {
                    var ClaimFormDeadline = "<div class=\"section\"><div class=\"menu\">Claim Form Deadline</div><div class=\"text\">" + DateTime.Parse(dr["ClaimFormDeadline"].ToString()).ToString("MMMM dd, yyyy") + "</div></div>";
                    litClaimFormDeadline.Text = ClaimFormDeadline;
                }

                if (Functions.ConvertToString(dr["URL"]).StartsWith("www."))
                {

                    lnkBtn.HRef = Functions.ConvertToString("http://" + dr["URL"]);
                }
                else
                {
                    lnkBtn.HRef = Functions.ConvertToString(dr["URL"]);
                }
                //lnkBtn.HRef = Functions.ConvertToString(dr["URL"]);
            }
        }

        dr = DB.DbFunctions.GetSettlementsPrevNext(id);
        if (dr != null)
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    HtmlAnchor lnk;
                    HtmlAnchor imgLnk;
                    HtmlImage img;

                    if (Convert.ToString(dr["Type"]) == "Prev")
                    {
                        lnk = lnkPrev;
                        imgLnk = imgLnkPrev;
                        img = imgPrev;
                    }
                    else
                    {
                        lnk = lnkNext;
                        imgLnk = imgLnkNext;
                        img = imgNext;
                    }

                    img.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagessettlementsrebatesurl"] + Functions.ConvertToString(dr["Thumbnail"]);
                    lnk.HRef = "/Rebates/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    imgLnk.HRef = lnk.HRef;
                }
            }
            dr.Close();
            dr.Dispose();
        }
    }
}