﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Content.aspx.cs" Inherits="Content" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            // Get on screen image
            var screenImage = $("#content .img");
            var theImage = new Image();
            theImage.onload = function () {
                $("#content .img").css("maxWidth", String(this.width) + "px");
            };
            theImage.src = screenImage.attr("src");

            $('#content .ckedit img').each(function (i) {
                $(this).css("maxWidth", $(this).css('width'));
                $(this).css('width', '100%');
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />

    <div id="content">
        <h1><asp:Literal runat="server" ID="litTitle"></asp:Literal></h1>
        <hr class="blueline">
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionTop"></asp:Literal></div>
        <asp:Literal runat="server" ID="litImage"></asp:Literal>
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionBtm"></asp:Literal></div>
    </div>
</asp:Content>
