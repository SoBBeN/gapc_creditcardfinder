﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="/css/news.css" rel="stylesheet" />
    <link href="/css/lifestyle.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            if (matchMedia('only screen and (min-width: 310px) and (max-width:500px)').matches) {
                //$('.ads_padding_middle').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- HMG - Body --><ins class="adsbygoogle" style="display:inline-block;width:98%;height:50px" data-ad-client="ca-pub-0634471641041185" data-ad-slot="9580178383" data-ad-format="auto"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div id="content">
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionTop"></asp:Literal></div>
        <asp:Literal runat="server" ID="litImage"></asp:Literal>
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionBtm"></asp:Literal></div>
<%--        <h1><a href="/Lifestyle"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/icons_homestyle.png" /><asp:Literal runat="server" ID="litLifestylesTitle">Latest in <span>MoneyTalk</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="lifestyles">
            <asp:Literal runat="server" ID="litArticles"></asp:Literal>
        </div>--%>

        <br class="clear" />
        <h1><a href="/News"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/icons_homemoneynews.png" /><asp:Literal runat="server" ID="litTitleAstroTalk">Latest in <span>Credit Card News</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="news">
            <asp:Literal runat="server" ID="litNews"></asp:Literal>
        </div>
        <br class="clear" />
        <h1><a href="/Products"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/icons_homeproducts.png" /><asp:Literal runat="server" ID="Literal1">Latest in <span>Find Credit Cards</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="news">
            <asp:Literal runat="server" ID="litProducts"></asp:Literal>
        </div>
        <br class="clear" />
<%--        <div class="ads_padding_top ads_padding_middle"></div>--%>
    </div>
    <div class="clear"></div>
</asp:Content>

