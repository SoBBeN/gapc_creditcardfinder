﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Terms : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlDataReader dr = DB.DbFunctions.GetWebSite();

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                litPrivacy.Text = Convert.ToString(dr["HowItWorks"]);
            }
            dr.Close();
            dr.Dispose();
        }
    }
}