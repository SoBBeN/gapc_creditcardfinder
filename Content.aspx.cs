﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Content : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		//Start Post to AIB
		var leadID = Request.QueryString["lid"];
		var utm_source = Request.QueryString["utm_source"];
		var utm_medium = Request.QueryString["utm_medium"];
		var utm_campaign = Request.QueryString["utm_campaign"];
		var s_id = Request.QueryString["s_id"];
		var email = Request.QueryString["e"];
		string ip_address = HttpContext.Current.Request.UserHostAddress;
		string source_sub_id = "organic";
		string referrer = "";

		if (Request.QueryString["referrer"] == "facebook")
		{

			referrer = Request.QueryString["referrer"];
			Session["PFG_referrer"] = referrer;
			source_sub_id = referrer;
		}

		if (utm_source == "AIB" && (email != null && email != ""))
		{
			DB.DbFunctions.PostToAllInbox("857", "", email, "", ip_address, "", "", utm_source);
		}

		var nvc = HttpUtility.ParseQueryString(Request.Url.Query);
		nvc.Remove("e");
		nvc.Remove("email");

		if (Request.QueryString["e"] != null || Request.QueryString["email"] != null)
		{
			Response.Redirect("Content.aspx?" + nvc.ToString());
		}
		//End Post to AIB



		int id;

        if (!int.TryParse(Convert.ToString(Request.QueryString["id"]), out id))
        {
            if (int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
                id = Convert.ToInt32(Page.RouteData.Values["id"]);
            else
                id = 7;
        }

        SqlDataReader dr;

        if (id > 0)
        {
            dr = DB.DbFunctions.GetContent(id);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimagescontentsurl"] + Functions.ConvertToString(dr["ImageFilename"]);
                        if (id != 7)
                            breadcrumbs1.AddLevel(Convert.ToString(dr["Category"]), "/content/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/");
                        ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["Title"]);
                        ((ITmgMasterPage)Master).PageURL = "content/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/";
                        ((ITmgMasterPage)Master).PageType = "article";
                        ((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["DescriptionTop"]) + " " + Convert.ToString(dr["DescriptionBtm"]);


                        litTitle.Text = Functions.ConvertToString(dr["Title"]);
                        litDescriptionTop.Text = Functions.ConvertToString(dr["DescriptionTop"]);
                        litDescriptionBtm.Text = Functions.ConvertToString(dr["DescriptionBtm"]);

                        if (dr["ImageFilename"] != null && Convert.ToString(dr["ImageFilename"]).Length > 0)
                        {
                            if (Functions.ConvertToString(dr["ImageLink"]).Length > 0)
                                litImage.Text = "<a href=\"" + Functions.ConvertToString(dr["ImageLink"]) + "\" target=\"_blank\">";
                            else
                                litImage.Text = "<a>";

                            litImage.Text += "<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["baseimagescontentsurl"] + Functions.ConvertToString(dr["ImageFilename"]) + "\" id=\"imgMain\" class=\"img\" />";
                            litImage.Text += "</a>";
                        }
                        else
                            litImage.Visible = false;
                    }
                }
                dr.Close();
                dr.Dispose();
            }
        }


        if (!Page.IsPostBack)
        {
            if (Request.QueryString["lid"] != null && Request.QueryString["lid"] != "")
            {
                dr = DB.DbFunctions.GetLeadData(int.Parse(Request.QueryString["lid"].ToString()));

                if (dr != null)
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string IpAddress = Request.UserHostAddress;
                            //DB.DbFunctions.PostToAllInbox("777", dr["SessionID"].ToString(), dr["emailaddress"].ToString(), dr["firstname"].ToString(), IpAddress, dr["lastname"].ToString(), dr["homephoneno"].ToString(), dr["referid"].ToString());
                        }
                    }
                }

                dr.Close();
                dr.Dispose();
            }
        }
    }
}