﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WebsiteAdd.aspx.cs" Inherits="WebsiteAdd" ValidateRequest="false" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDescription" AssociatedControlID="txtDescription" Text="Description:" />
                </td>
                <td>
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="text" Columns="50" MaxLength="50" Width="300px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtDescription" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDisclaimer1" AssociatedControlID="txtDisclaimer1" Text="Disclaimer 1:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="txtDisclaimer1" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtDisclaimer1.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDisclaimer2" AssociatedControlID="txtDisclaimer2" Text="Disclaimer 2:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="txtDisclaimer2" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtDisclaimer2.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDisclaimer3" AssociatedControlID="txtDisclaimer3" Text="Disclaimer 3:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="txtDisclaimer3" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtDisclaimer3.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDisclaimer4" AssociatedControlID="txtDisclaimer4" Text="Disclaimer 4:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="txtDisclaimer4" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtDisclaimer4.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblPrivacy" AssociatedControlID="txtPrivacy" Text="Privacy Policy:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="txtPrivacy" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtPrivacy.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblTerms" AssociatedControlID="txtTerms" Text="Terms & Conditions:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="txtTerms" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtTerms.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>

             <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblHowItWorks" AssociatedControlID="TxtHowItWorks" Text="How It Works" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="TxtHowItWorks" runat="server" Width="800px" Height="300px"></asp:TextBox>
                </td>
            </tr>


             <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblPrivacyRequestForm" AssociatedControlID="TxtPrivacyRequestForm" Text="PrivacyRequestForm" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="TxtPrivacyRequestForm" runat="server" Width="800px" Height="300px"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblPrivacyNotice" AssociatedControlID="TxtPrivacyNotice" Text="PrivacyNotice" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="TxtPrivacyNotice" runat="server" Width="800px" Height="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblAccessibilityNotice" AssociatedControlID="TxtAccessibilityNotice" Text="AccessibilityNotice" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="TxtAccessibilityNotice" runat="server" Width="800px" Height="300px"></asp:TextBox>
                </td>
            </tr>




            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblContentHeader" AssociatedControlID="txtTerms" Text="Content Site Header Desktop:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txtContentHeader" runat="server" MaxLength="500" Width="800"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblContentHeaderMobile" AssociatedControlID="txtTerms" Text="Content Site Header Mobile:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txtContentHeaderMobile" runat="server" MaxLength="500" Width="800"></asp:TextBox>
                </td>
            </tr>
           <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblMenuText" AssociatedControlID="txtTerms" Text="Menu Text:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txtMenu" runat="server" MaxLength="500" Width="800"></asp:TextBox>
                </td>
            </tr>
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" />
    </div></asp:Content>

