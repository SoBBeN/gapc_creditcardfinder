﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminsAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "User";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EditUsers"] == null || Convert.ToBoolean(Session["EditUsers"]) != true)
        {
            Response.Write("You don't have access to this section");
            Response.End();
            return;
        }

        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
        }
    }

    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetAdmin(id);

        if (dr.HasRows)
        {
            dr.Read();
            txtUsername.Text = Convert.ToString(dr["Username"]);
            chkActive.Checked = Convert.ToBoolean(dr["IsActive"]);
            chkEditUsers.Checked = Convert.ToBoolean(dr["EditUsers"]);
            btnSave.Text = "Update " + ITEMNAME;
            RequiredFieldValidatorPassword.Enabled = false;
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
        }
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (txtPassword.Text != txtPassword2.Text)
            {
                divMsg.InnerHtml = "Error: Passwords doesn't match.";
                divMsg.Visible = true;
            }
            else
            {
                int id = 0;

                if (hidID.Value.Length == 0) //INSERT
                {
                    id = DB.DbFunctions.InsertAdmin(txtUsername.Text.Trim(), txtPassword.Text, chkActive.Checked, chkEditUsers.Checked);

                    //POLL INSERTED SUCCESSFULLY
                    if (id > 0)
                    {
                        hidID.Value = id.ToString();
                        divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                        divMsg.Visible = true;
                    }
                }
                else //UPDATE
                {
                    id = int.Parse(hidID.Value);
                    DB.DbFunctions.UpdateAdmin(id, txtUsername.Text.Trim(), txtPassword.Text, chkActive.Checked, chkEditUsers.Checked);

                    divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                    divMsg.Visible = true;

                }
                //ShowExistingValues();
            }
        }
    }
}