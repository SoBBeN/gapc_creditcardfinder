﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PointsOffersAdd.aspx.cs" Inherits="PointsOffersAdd" ValidateRequest="false" %>
<%@ Register Assembly="MyClassLibrary" Namespace="MyClassLibrary.Controls" TagPrefix="CustomControl" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <link href="http://code.jquery.com/ui/1.9.2/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("#<%=txtDay.ClientID %>").datepicker({
                dateFormat: "mm/dd/yy",
                changeMonth: true,
                changeYear: true
            });
            $("#<%=txtDayExpiry.ClientID %>").datepicker({
                dateFormat: "mm/dd/yy",
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
	<style>
	  .selectable .ui-selecting { background: #FECA40; }
	  .selectable .ui-selected { background: #F39814; color: white; }
	  .selectable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	  .selectable li { margin: 3px; padding: 1px; float: left; width: 183px; height: 25px; font-size: 18px; font-weight:bold; line-height:25px; text-align: center; }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblImageFile" AssociatedControlID="ImageFile" Text="Image File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="ImageFile" />
                </td>
                <div style="float:right; overflow:visible; height:0px; position:relative; right:0px; top:-50px;">
                    <img runat="server" id="imgImageFile" style="max-height:225px; max-width:350px;" />
                    <img runat="server" id="imgThumbnail" style="max-height:225px; max-width:350px;" />
                </div>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblThumbnail" AssociatedControlID="ImageFile" Text="Thumbnail File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="fileThumbnail" /> <a runat="server" id="lnkThumbnail">Edit from Image</a>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblName" AssociatedControlID="txtTitle" Text="Name:" />
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" CssClass="text" Columns="100" Width="400px" />&nbsp;
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblTitle" AssociatedControlID="txtTitle" Text="Title:" />
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="text" Columns="100" Width="400px" />&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblLinkURL" AssociatedControlID="txtLinkURL" Text="Link:" />
                </td>
                <td>
                    <asp:TextBox ID="txtLinkURL" runat="server" CssClass="text" Columns="150" MaxLength="150" Width="250px" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtLinkURL" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblPoints" AssociatedControlID="txtPoints" Text="Points:" />
                </td>
                <td>
                    <asp:TextBox ID="txtPoints" runat="server" CssClass="text" Columns="100" Width="100px" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDescription" AssociatedControlID="txtText" Text="Text:" />
                </td>
                <td>
                    <CKEditor:CKEditorControl ID="txtText" BasePath="ckeditor/" runat="server" Width="800px" Height="150px"></CKEditor:CKEditorControl>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblCategory" AssociatedControlID="sltCategory" Text="Other Categories:" />
                </td>
                <td>
                    <hr />
                    <CustomControl:Selectable ID='sltCategory' runat="server"></CustomControl:Selectable>
                    <div style="clear:both;"></div>
                    <asp:TextBox ID="txtAddCat" runat="server" CssClass="text" Columns="50" MaxLength="50" /><asp:Button ID="btnCategory" runat="server" Text="Add Category" OnClick="btnCategory_Click" CssClass="submit" />
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblActiveAll" AssociatedControlID="chkActiveAll" Text="Main Active (All Websites):" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActiveAll" Checked="false" /> (When checked, offer is live on all websites below)
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDayExpiry" AssociatedControlID="txtDayExpiry" Text="Expiry Date for all websites:" />
                </td>
                <td>
                    <asp:TextBox ID="txtDayExpiry" runat="server" CssClass="text" Columns="10" MaxLength="10" />&nbsp;
                </td>
            </tr>
            <tr><td>&nbsp;</td><td><hr /></td></tr>
            <tr>
                <td style="white-space:nowrap">
                    <asp:Label runat="server" ID="lblActive" AssociatedControlID="chkActive" Text="Active Status (This Website):" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActive" Checked="false" />
                </td>
            </tr>
            <tr>
                <td style="white-space:nowrap">
                    <asp:Label runat="server" ID="lblDay" AssociatedControlID="txtDay" Text="Active Date:" />
                </td>
                <td>
                    <asp:TextBox ID="txtDay" runat="server" CssClass="text" Columns="10" MaxLength="10" />&nbsp;
                </td>
            </tr>
            <asp:Literal runat="server" ID="litMoreWebSite" />
            <tr>
                <td>
                    Rewards Link:
                </td>
                <td>
                    <a target="_blank" runat="server" id="lnkLink">See Preview</a>
                </td>
            </tr>
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" /> &nbsp; &nbsp; &nbsp; 
        <a target="_blank" runat="server" id="lnkPreview" visible="false">See Preview</a>
        
    </div>
</asp:Content>

