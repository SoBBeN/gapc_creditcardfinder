﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LifestyleAdd.aspx.cs" Inherits="LifestyleAdd" %>
<%@ Register Assembly="MyClassLibrary" Namespace="MyClassLibrary.Controls" TagPrefix="CustomControl" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/start/jquery-ui.css" />
    <script type="text/javascript">
        $(function () {
            $("#<%=txtDay.ClientID %>").datepicker({
                dateFormat: "mm/dd/yy",
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
	<style>
	  .selectable .ui-selecting { background: #FECA40; }
	  .selectable .ui-selected { background: #F39814; color: white; }
	  .selectable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	  .selectable li { margin: 3px; padding: 1px; float: left; width: 183px; height: 25px; font-size: 18px; font-weight:bold; line-height:25px; text-align: center; }
	</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <table width="100%">
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblTitle" AssociatedControlID="txtTitle" Text="Title:" />
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="text" Columns="100" Width="400px" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTitle" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtTitle" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblSubTitle" AssociatedControlID="txtSubtitle" Text="SubTitle:" />
                </td>
                <td>
                    <asp:TextBox ID="txtSubtitle" runat="server" CssClass="text" Columns="100" Width="400px" MaxLength="250" />&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Slides
                </td>
                <td>
                    <a id="lnkNewSlide" runat="server" href="" disabled="disabled">Add New Slide (Save first)</a>
                    <br />
                    <div id="right">
                        <div runat="server" id="div1" class="mInfo" visible="false">
                        </div>
                        <asp:GridView ID="gv" runat="server" Width="100%" GridLines="None" DataKeyNames="ID"
                            OnRowDeleting="GvRowDeleting" CssClass="grid" OnRowCommand="GvRowCommand">
                            <HeaderStyle CssClass="gridHead" />
                            <Columns>
                                <asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="LifestyleSlideAdd.aspx?id={0}"
                                    Text="Edit" />
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Delete"
                                            Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this lifestyle slide ?')" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="false" CommandName="MoveUp"
                            Text="Move Up" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="false" CommandName="MoveDown"
                            Text="Move Down" />
                    </ItemTemplate>
                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblCategory" AssociatedControlID="sltCategoryTmg" Text="Categories:" />
                </td>
                <td>
                    <fieldset runat="server" id="fsTMG">
                        <legend>The Mommy Guide</legend>
                        <CustomControl:Selectable ID='sltCategoryTmg' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <br />
                    <fieldset runat="server" id="fsPG" visible="false">
                        <legend>The Pet Guide</legend>
                        <CustomControl:Selectable ID='sltCategoryTpg' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <br />
                    <fieldset runat="server" id="fsCI" visible="false">
                        <legend>Coupon Insanity / The Coupons Guide</legend>
                        <CustomControl:Selectable ID='sltCategoryCI' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <br />
                    <fieldset runat="server" id="fsFMG" visible="false">
                        <legend>Found Money Guide</legend>
                        <CustomControl:Selectable ID='sltFMG' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <br />
                    <fieldset runat="server" id="fsC4R">
                        <legend>LoveHopeLuck</legend>
                        <CustomControl:Selectable ID='sltCategoryC4R' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <br />
                    <fieldset runat="server" id="fsTAG">
                        <legend>TheAstrologyGuide</legend>
                        <CustomControl:Selectable ID='sltCategoryTAG' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <br />
                    <fieldset runat="server" id="fsHMG">
                        <legend>TheHomeMoneyGuide</legend>
                        <CustomControl:Selectable ID='sltCategoryHMG' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <br />
                    <fieldset runat="server" id="fsFSG">
                        <legend>TheFreeSampleGuide</legend>
                        <CustomControl:Selectable ID='sltCategoryFSG' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <br />
                    <fieldset runat="server" id="Fieldset1">
                        <legend>ThePersonalFinancialGuide</legend>
                        <CustomControl:Selectable ID='sltCategoryPFG' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <fieldset runat="server" id="fsTWM">
                        <legend>The Winners Market</legend>
                        <CustomControl:Selectable ID='sltCategoryTWM' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <fieldset runat="server" id="Fieldset2">
                        <legend>My Coupon Saver</legend>
                        <CustomControl:Selectable ID='sltCategoryMCS' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    </fieldset>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblTag" AssociatedControlID="sltTag" Text="Tags:" />
                </td>
                <td>
                    <hr />
                        <CustomControl:Selectable ID='sltTag' runat="server"></CustomControl:Selectable>
                        <div style="clear:both;"></div>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblActive" AssociatedControlID="chkActive" Text="Active Status (This Website):" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActive" Checked="false" /> (When unchecked, only visible on dev)
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDay" AssociatedControlID="txtDay" Text="Active Date:" />
                </td>
                <td>
                    <asp:TextBox ID="txtDay" runat="server" CssClass="text" Columns="10" MaxLength="10" />
                </td>
            </tr>
            <asp:Literal runat="server" ID="litMoreWebSite" />
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" /> &nbsp; &nbsp; &nbsp; 
        <a target="_blank" runat="server" id="lnkPreview" visible="false">See Preview</a>
    </div>
</asp:Content>
