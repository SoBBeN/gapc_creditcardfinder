﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InternalAds : System.Web.UI.Page
{
    protected const string ITEMNAME = "Internal Ads";

    protected void Page_PreRender(object sender, EventArgs e)
    {
        divMsg.Visible = false;
        if (!IsPostBack)
        {
            FillGV();
        }
    }

    private void FillGV()
    {
        SqlDataReader dt = DB.DbFunctions.GetAllInternalAds();

        gv.DataSource = dt;
        gv.DataBind();
    }

    protected void GvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gv.DataKeys[e.RowIndex]["ID"].ToString());

        DB.DbFunctions.DeleteInternalAds(id);

        divMsg.InnerHtml = "Selected " + ITEMNAME + " deleted successfully.";
        divMsg.Visible = true;

        FillGV();
    }

    protected void GvRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
        int RowIndex = gvr.RowIndex;
        int id = int.Parse(gv.DataKeys[RowIndex]["ID"].ToString());
        bool moveup = (e.CommandName == "MoveUp");

        DB.DbFunctions.MoveInternalAds(id, moveup);
        FillGV();
    }
}