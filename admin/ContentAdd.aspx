﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContentAdd.aspx.cs" Inherits="ContentAdd" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/start/jquery-ui.css" />
    <script src="ckeditor446/ckeditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblImageFile" AssociatedControlID="ImageFile" Text="Image File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="ImageFile" /> <asp:LinkButton Visible="false" runat="server" ID="lnkRemoveImg" OnClick="lnkRemoveImg_Click">Remove Image</asp:LinkButton>
                </td>
                <div style="float:right; overflow:visible; height:0; position:relative; right:0; top:-50px;">
                    <img runat="server" id="imgImageFile" style="max-height:225px; max-width:350px;" />
                </div>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblImageLink" AssociatedControlID="txtImageLink" Text="Image URL:" />
                </td>
                <td>
                    <asp:TextBox ID="txtImageLink" runat="server" CssClass="text" Columns="100" Width="400px" MaxLength="250" />&nbsp;
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblTitle" AssociatedControlID="txtTitle" Text="Title:" />
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="text" Columns="100" Width="400px" MaxLength="255" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTitle" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtTitle" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblCategory" AssociatedControlID="txtCategory" Text="Category:" />
                </td>
                <td>
                    <asp:TextBox ID="txtCategory" runat="server" CssClass="text" Columns="100" Width="400px" MaxLength="255" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtCategory" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDescriptionTop" AssociatedControlID="txtDescriptionTop" Text="Before Image:" />
                </td>
                <td>
                    <asp:TextBox TextMode="MultiLine" ID="txtDescriptionTop" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtDescriptionTop.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>

                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDescription" AssociatedControlID="txtDescription" Text="After Image:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="txtDescription" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtDescription.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblActive" AssociatedControlID="chkActive" Text="Active Status :" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActive" Checked="false" /> (When unchecked, only visible on dev)
                </td>
            </tr>
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" /> &nbsp; &nbsp; &nbsp; 
        <a target="_blank" runat="server" id="lnkPreview" visible="false">See Preview</a>
    </div>
</asp:Content>

