﻿using DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for UserSession
/// </summary>
public class UserSession
{
	private int sessionId;
    private Guid guid;
    private int campaignId;
    private string subId;
    private string affId;
    private DataTable babyNameSearchList;
    private int babyNameSearchListLastMax;

    public UserSession()
    {
        InitializeSession();
    }

    public int SessionID
    {
        get { return sessionId; }
    }
    public Guid Guid
    {
        get { return guid; }
    }
    public int CampaignID
    {
        get { return campaignId; }
    }
    public string SubID
    {
        get { return subId; }
    }
    public string AffiliateID
    {
        get { return affId; }
    }

    public DataTable BabyNameSearchList
    {
        get { return babyNameSearchList; }
        set { babyNameSearchList = value; }
    }
    public int BabyNameSearchListLastMax
    {
        get { return babyNameSearchListLastMax; }
        set { babyNameSearchListLastMax = value; }
    }

    public static void InsertImpression()
    {
        GetInstance(true);
    }

	public static UserSession GetInstance()
	{
        return GetInstance(false);
    }

	public static UserSession GetInstance(bool insertImpression)
	{
        UserSession instance = null;

        if ((HttpContext.Current.Session["UserSession"] != null))
        {
            instance = (UserSession)HttpContext.Current.Session["UserSession"];
		} else {
            instance = new UserSession();
            HttpContext.Current.Session["UserSession"] = instance;
		}

        if (insertImpression)
            instance.PrivateInsertImpression();

		return instance;
	}

    private void InitializeSession()
    {
            string strReferrer = GetDefaultValue(HttpContext.Current.Request.QueryString, "r", "");
            subId = GetDefaultValue(HttpContext.Current.Request.QueryString, "subid", "");
            string strUrlReferrer = HttpContext.Current.Request.UrlReferrer == null ? "" : HttpContext.Current.Request.UrlReferrer.ToString();
            affId = GetDefaultValue(HttpContext.Current.Request.QueryString, "affId", "");
            string hitId = GetDefaultValue(HttpContext.Current.Request.QueryString, "hitId", "");
            guid = Guid.NewGuid();

            if (!int.TryParse(HttpContext.Current.Request.QueryString.Get("c"), out campaignId))
                campaignId = 0;

            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_InsertSession] ");

            query.Append(objSqlServer.FormatSql(guid.ToString())).Append(",");
            query.Append(objSqlServer.FormatSql(campaignId)).Append(",");
            query.Append(objSqlServer.FormatSql(strReferrer)).Append(",");
            query.Append(objSqlServer.FormatSql(subId)).Append(",");
            query.Append(objSqlServer.FormatSql(strUrlReferrer)).Append(",");
            query.Append(objSqlServer.FormatSql(HttpContext.Current.Request.UserAgent)).Append(",");
            query.Append(objSqlServer.FormatSql(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"])).Append(",");
            query.Append(objSqlServer.FormatSql(affId)).Append(",");
            query.Append(objSqlServer.FormatSql(hitId)).Append(",");
            query.Append(objSqlServer.FormatSql(HttpContext.Current.Request.RawUrl));

            sessionId = Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
    }

    private static string GetDefaultValue(System.Collections.Specialized.NameValueCollection Collection, string Name, string Default)
    {
        return Collection[Name] == null ? Default : Collection[Name];
    }

    private void PrivateInsertImpression()
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[BF_InsertImpression] ");

        query.Append(objSqlServer.FormatSql(HttpContext.Current.Request.UrlReferrer == null ? null : HttpContext.Current.Request.UrlReferrer.ToString())).Append(",");
        query.Append(objSqlServer.FormatSql(GetPage())).Append(",");
        query.Append(objSqlServer.FormatSql(SessionID));

        objSqlServer.ExecNonQuery(query.ToString());
    }

    private static string GetPage()
    {
        string url = HttpContext.Current.Request.Url.AbsolutePath.ToLower();

        if (HttpContext.Current.Request.Url.Host == "localhost" && url.ToLower().StartsWith("/babyfreebie"))
            url = url.Substring(12);

        if (url.EndsWith("/"))
            url += "default.aspx";

        return url;
    }
}