﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for Error
/// </summary>
public class ErrorHandling
{
    private const string APPLICATION_NAME = "TopCreditCardfinderAdmin";

    private static bool LogException(ref Exception ex)
    {
        EventLog objEventLog = new EventLog();
        const string appName = APPLICATION_NAME;
        try
        {
            //Register the Application as an Event Source
            if (!EventLog.SourceExists(appName))
            {
                EventLog.CreateEventSource(appName, "Application");
            }

            //log the entry
            objEventLog.Source = appName;
            objEventLog.WriteEntry(ex.Message, EventLogEntryType.Error);

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public static void SendException(string subject, Exception ex)
    {
        SendException(subject, ex, String.Empty, true);
    }

    public static void SendException(string subject, Exception ex, string suppMessage)
    {
        SendException(subject, ex, suppMessage, true);
    }

    private static void SendException(string subject, Exception ex, string suppMessage, bool firstTime)
    {
//#if DEBUG
//            throw ex;
//#else
        StringBuilder message = new StringBuilder();
        try
        {

            if (((suppMessage != null)))
            {
                message.Append(Environment.NewLine + suppMessage + Environment.NewLine + Environment.NewLine);
            }

            message.Append("Exception Info:" + Environment.NewLine);
            message.Append("Message:" + ex.Message + Environment.NewLine);
            message.Append("Source:" + ex.Source + Environment.NewLine);
            message.Append("Stack Trace:" + ex.StackTrace + Environment.NewLine);

            if (ex is WebException)
            {
                WebException webEx = default(WebException);
                webEx = (WebException)ex;
                WebResponse response = webEx.Response;
                if (response != null)
                {
                    message.Append("Headers:" + Environment.NewLine);
                    if (response.Headers != null)
                    {
                        foreach (string headerName in response.Headers)
                        {
                            message.Append(headerName + ":" + response.Headers.Get(headerName) + Environment.NewLine);
                        }
                    }
                    message.Append(Environment.NewLine);
                    try
                    {
                        StreamReader reader = new StreamReader(response.GetResponseStream());
                        message.Append("WebResponse:" + reader.ReadToEnd() + Environment.NewLine);
                    }
                    catch (Exception ex2)
                    {
                        message.Append("WebResponse (Error):" + ex2.Message + Environment.NewLine);
                    }
                }
                else
                {
                    message.Append("WebResponse Is Null:" + Environment.NewLine);
                }
            }
            else if (ex is SqlException)
            {
                SqlException sqlEx = default(SqlException);
                sqlEx = (SqlException)ex;
                message.Append("SqlServer:" + sqlEx.Server + Environment.NewLine);
                message.Append("Procedure:" + sqlEx.Procedure + Environment.NewLine);
                message.Append("LineNumber:" + Convert.ToString(sqlEx.LineNumber) + Environment.NewLine);
                message.Append("Sql Error Number:" + Convert.ToString(sqlEx.Number) + Environment.NewLine);
            }

            if (((ex.InnerException != null)))
            {
                message.Append(Environment.NewLine + "InnerException Info:" + Environment.NewLine);
                message.Append("Message:" + ex.InnerException.Message + Environment.NewLine);
                message.Append("Source:" + ex.InnerException.Source + Environment.NewLine);
                message.Append("Stack Trace:" + ex.InnerException.StackTrace + Environment.NewLine);
            }

            SmtpClient client = new SmtpClient();
            client.Send("donotreply@click4riches.com", "mathieug@netspheresolutions.com", APPLICATION_NAME + " Error " + subject, message.ToString());

            LogException(ref ex);

        }
        catch (Exception exEx)
        {
            string firstmessage = message.ToString();
            if (firstTime)
            {
                SendException("Exception In Exception", exEx, ex.ToString() + Environment.NewLine + Environment.NewLine + suppMessage, false);
            }
            else
            {
                SmtpClient client = new SmtpClient();
                try
                {
                    client.Send("donotreply@click4riches.com", "mathieug@netspheresolutions.com", APPLICATION_NAME + " Error  " + subject, ex.ToString() + Environment.NewLine + Environment.NewLine + suppMessage + Environment.NewLine + Environment.NewLine + exEx.ToString());
                }
                catch
                {
                    HttpContext.Current.Response.Write("An unhandled exception occurred during the execution of the current web request and cannot be sent by email to the administrator. Please send the following message to the administrator by email:<br /><br />");

                    HttpContext.Current.Response.Write(firstmessage.ToString().Replace("\n", "<br />"));
                    HttpContext.Current.Response.End();
                }
            }
        }
//#endif
    }

}