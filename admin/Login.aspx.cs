﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            if (string.IsNullOrEmpty(Request.QueryString["Logout"]))
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
            }

            HttpCookie cookie_u = Request.Cookies["log_info_u"];
            HttpCookie cookie_p = Request.Cookies["log_info_p"];

            if ((((cookie_u != null)) & ((cookie_p != null))))
            {
                tbUsername.Text = cookie_u.Value;
                tbPassword.Attributes.Add("Value", cookie_p.Value);
            }
        }
    }

    protected void btLogin_Click(object sender, System.EventArgs e)
    {
        bool isLoginValid = false;
        //int adminID = 0;
        DataRow dr = null;
        try
        {
            dr = DB.DbFunctions.Authenticate(tbUsername.Text, tbPassword.Text);
            isLoginValid = (dr != null);
            //isLoginValid = FormsAuthentication.Authenticate(tbUsername.Text.ToLower(), tbPassword.Text);
        }
        catch (Exception ex)
        {
            ErrorHandling.SendException("btLogin_Click", ex, "Login");
        }
        if (isLoginValid)
        {
            Session.Timeout = 90;
            //Session["Username"] = tbUsername.Text;
            //Session["AdminID"] = adminID;

            Session["Username"] = dr["Username"];
            Session["AdminID"] = Convert.ToInt32(dr["ID"]);
            Session["EditUsers"] = Convert.ToBoolean(dr["EditUsers"]);
            Session["MenuSection"] = string.Empty;
            
            FormsAuthentication.SetAuthCookie(tbUsername.Text, false);
            if (chk_cookie.Checked)
            {
                rememberInfo();
            }
            else
            {
                clearCookiesInfo();
            }
            string redirectUrl = "Default.aspx";
            if ((Request.QueryString["ReturnUrl"] != null))
            {
                if (!String.IsNullOrWhiteSpace(Request.QueryString["ReturnUrl"]))
                {
                    redirectUrl = Request.QueryString["ReturnUrl"];
                }
            }
            Response.Redirect(redirectUrl);
        }
        else
        {
            errMsg.Text = "Username and password do not match";
        }

    }

    private void clearCookiesInfo()
    {
        HttpCookie log_cookie_u = new HttpCookie("log_info_u");
        HttpCookie log_cookie_p = new HttpCookie("log_info_p");
        log_cookie_u.Expires = DateTime.Now;
        log_cookie_p.Expires = DateTime.Now;
        Response.AppendCookie(log_cookie_u);
        Response.AppendCookie(log_cookie_p);
    }

    private void rememberInfo()
    {
        HttpCookie log_cookie_u = new HttpCookie("log_info_u");
        HttpCookie log_cookie_p = new HttpCookie("log_info_p");

        log_cookie_u.Value = tbUsername.Text;
        log_cookie_p.Value = tbPassword.Text;
        log_cookie_u.Expires = DateTime.Now.AddDays(60);
        log_cookie_p.Expires = DateTime.Now.AddDays(60);

        Response.AppendCookie(log_cookie_u);
        Response.AppendCookie(log_cookie_p);
    }


}