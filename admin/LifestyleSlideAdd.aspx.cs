﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LifestyleSlideAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Luck Letter Slide";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            FillDdl();
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["fid"]))
                ddlArticle.SelectedValue = Request.QueryString["fid"];

            int recipeid;
            if (!int.TryParse(hidID.Value, out recipeid))
                recipeid = int.MinValue;
        }
    }

    private void FillDdl()
    {
        SqlDataReader dr = DB.DbFunctions.GetAllLifestyle();
        ddlArticle.DataSource = dr;
        ddlArticle.DataTextField = "Title";
        ddlArticle.DataValueField = "ID";
        ddlArticle.DataBind();
        dr.Close();
    }

    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetLifestyleSlide(id);

        if (dr.HasRows)
        {
            dr.Read();

            ddlArticle.SelectedValue = Functions.ConvertToString(dr["LifestyleID"]);
            txtSubtitle.Text = Functions.ConvertToString(dr["SubTitle"]);
            txtDescription.Text = Functions.ConvertToString(dr["Description"]);
            txtDescriptionTop.Text = Functions.ConvertToString(dr["DescriptionTop"]);
            txtDay.Text = Functions.ConvertToString(dr["ActiveDate"]);
            chkActive.Checked = Convert.ToBoolean(dr["IsActive"]);
            lnkPreview.HRef = System.Configuration.ConfigurationManager.AppSettings["basepreviewurl"] + "Lifestyle/" + Functions.ConvertToString(dr["LifestyleID"]) + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Category"])) + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Title"])) + "/" + id + "/";
            imgImageFile.Src = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Functions.ConvertToString(dr["ImageFilename"]);
            imgThumbnail.Src = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Functions.ConvertToString(dr["Thumbnail"]);
            lnkPreview.Visible = true;
            btnSave.Text = "Update " + ITEMNAME;
            chkImageVisible.Checked = Convert.ToBoolean(dr["ImageVisible"]);
            txtVideo.Text = Functions.ConvertToString(dr["VideoScript"]);
            
            lnkThumbnail.HRef = "MomentsThumbnail.aspx?type=lf&id=" + hidID.Value + "&f=" + HttpUtility.UrlEncode(Functions.ConvertToString(dr["ImageFilename"]));
            if (dr["Thumbnail"] != DBNull.Value)
                lnkThumbnail.HRef += "&t=" + HttpUtility.UrlEncode(Functions.ConvertToString(dr["Thumbnail"]));
            lnkThumbnail.Disabled = false;
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
            if (!String.IsNullOrEmpty(Request.QueryString["fid"]))
                ddlArticle.SelectedValue = Request.QueryString["fid"];
        }
        dr.Close();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path) + "?fid=" + Convert.ToInt32(ddlArticle.SelectedValue));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int id = 0;

            string filename = String.Empty;
            if (ImageFile.HasFile)
            {
                filename = Functions.RemoveSpecialChars(ImageFile.FileName);

                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", "images/lifestyle", ref filename, ImageFile.PostedFile.InputStream);
            }

            string thumbnail = String.Empty;
            if (fileThumbnail.HasFile)
            {
                thumbnail = Functions.RemoveSpecialChars(fileThumbnail.FileName);
                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", "images/lifestyle", ref thumbnail, fileThumbnail.PostedFile.InputStream);
            }

            string activeDate;
            if (txtDay.Text.Length > 7)
                activeDate = txtDay.Text;
            else
                activeDate = null;

            if (hidID.Value.Length == 0) //INSERT
            {
                id = DB.DbFunctions.InsertLifestyleSlide(filename, Convert.ToInt32(ddlArticle.SelectedValue), txtSubtitle.Text, txtDescription.Text, chkActive.Checked, activeDate, thumbnail, txtDescriptionTop.Text, chkImageVisible.Checked, txtVideo.Text);

                //POLL INSERTED SUCCESSFULLY
                if (id > 0)
                {
                    hidID.Value = id.ToString();
                    divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                    divMsg.Visible = true;
                }
            }
            else //UPDATE
            {
                id = int.Parse(hidID.Value);
                DB.DbFunctions.UpdateLifestyleSlide(id, Convert.ToInt32(ddlArticle.SelectedValue), filename, txtSubtitle.Text, txtDescription.Text, chkActive.Checked, activeDate, thumbnail, txtDescriptionTop.Text, chkImageVisible.Checked, txtVideo.Text);

                divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                divMsg.Visible = true;
            }

            if (id > 0)
            {
                ShowExistingValues();
            }
        }
    }
}