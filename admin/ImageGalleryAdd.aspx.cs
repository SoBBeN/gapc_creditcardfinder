﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ImagesAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Image";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
        }
    }

    public void CustomValidator_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        args.IsValid = !(txtUrl.Text.Length < 16 && !ImageFile.HasFile);
    }

    private void ShowExistingValues()
    {
        SqlDataReader dr = DB.DbFunctions.GetImageGallery(int.Parse(hidID.Value));

        if (dr.HasRows)
        {
            dr.Read();
            //(ddlType.SelectedValue), txtTitle.Text, filename, 
            CustomValidator1.Enabled = false;
            imgImageFile.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagegalleryurl"] + Functions.ConvertToString(dr["ImageFilename"]);
            txtUrl.Text = System.Configuration.ConfigurationManager.AppSettings["baseimagegalleryurl"] + Functions.ConvertToString(dr["ImageFilename"]);
            btnSave.Visible = false;
            btnSaveAdd.Visible = false;


            System.Net.WebClient wc = new System.Net.WebClient();
            byte[] bytes = wc.DownloadData(System.Configuration.ConfigurationManager.AppSettings["baseimagegalleryurl"] + Functions.ConvertToString(dr["ImageFilename"]));
            MemoryStream ms = new MemoryStream(bytes);

            System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
            txtHeight.Text = img.Height.ToString();
            txtWidth.Text = img.Width.ToString();
            lblSize.Text = "Size";
            ImageFile.Visible = false;
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
        }
        dr.Close();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int id = 0;
            string filename = String.Empty;
            string path = System.Configuration.ConfigurationManager.AppSettings["baseimagegalleryurl"];

            if (ImageFile.HasFile)
            {
                filename = Functions.RemoveSpecialChars(ImageFile.FileName);

                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", "images/img", ref filename, ImageFile.PostedFile.InputStream);
                Resize(filename);
            }
            else
            {
                string url = txtUrl.Text.ToLower();
                if (url.EndsWith(".jpg") || url.EndsWith(".jpeg") || url.EndsWith(".png") || url.EndsWith(".gif") || url.Contains("?"))
                {
                    byte[] data;
                    using (WebClient client = new WebClient())
                    {
                        data = client.DownloadData(url);
                    }

                    if (url.Contains("?"))
                    {
                        filename = CreateRandomString(8);

                        System.Drawing.Image img;
                        try
                        {
                            MemoryStream stream = new MemoryStream(data);
                            img = System.Drawing.Image.FromStream(stream);
                        }
                        catch (Exception ex)
                        {
                            divMsg.InnerHtml = "Impossible to retrieve image <!--" + ex.ToString() + "-->";
                            divMsg.Visible = true;
                            return;
                        }

                        ImageFormat imgFormat;
                        if (img.RawFormat.Equals(ImageFormat.Gif))
                        {
                            imgFormat = ImageFormat.Gif;
                            filename += ".gif";
                        }
                        else if (img.RawFormat.Equals(ImageFormat.Png))
                        {
                            imgFormat = ImageFormat.Png;
                            filename += ".png";
                        }
                        else
                        {
                            imgFormat = ImageFormat.Jpeg;
                            filename += ".jpg";
                        }
                        filename = Functions.GetNonExistingFilename(path, filename);

                        MemoryStream ms = new MemoryStream();
                        img.Save(ms, imgFormat);

                        var azureStorage = new AzureStorage("AzureStorageConnection");
                        var blob = azureStorage.UploadBlog("themommyguide", "images/img", ref filename, ms);
                    }
                    else
                    {
                        filename = HttpUtility.UrlDecode(url.Substring(url.LastIndexOf("/") + 1));
                        filename = Functions.GetNonExistingFilename(path, filename);

                        var azureStorage = new AzureStorage("AzureStorageConnection");
                        var blob = azureStorage.UploadBlog("themommyguide", "images/img", ref filename, data);
                    }
                    Resize(filename);
                }
            }

            if (hidID.Value.Length == 0) //INSERT
            {
                if (filename.Length > 0)
                {
                    id = DB.DbFunctions.InsertImageGallery(filename);

                    //POLL INSERTED SUCCESSFULLY
                    if (id > 0)
                    {
                        hidID.Value = id.ToString();
                        divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                        divMsg.Visible = true;
                    }
                    ShowExistingValues();
                }
            }
            //else //UPDATE
            //{
            //    id = int.Parse(hidID.Value);
            //    DB.DbFunctions.UpdateImage(id, Convert.ToInt16(ddlType.SelectedValue), txtTitle.Text, filename, chkActive.Checked ? 1 : 0, txtText.Text, activeDate, null, null);

            //    divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
            //    divMsg.Visible = true;
            //}
        }
    }

    private static string CreateRandomString(int stringLength)
    {
        string allowedChars = "abcdefghijklmnopqrstuvwxyz0123456789";
        char[] chars = new char[stringLength];
        Random rd = new Random();

        for (int i = 0; i < stringLength; i++)
        {
            chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
        }

        return new string(chars);
    }

    protected void Resize(string sourceFile)
    {
        string sourcePath = System.Configuration.ConfigurationManager.AppSettings["baseimagegalleryurl"];
        int w; int h;

        if (Int32.TryParse(txtWidth.Text, out w))
        {
            if (!Int32.TryParse(txtHeight.Text, out h))
                h = -1;
            byte[] resizedImage = Resize(Path.Combine(sourcePath, sourceFile), w, h);

            if (resizedImage != null)
            {
                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", "images/img", ref sourceFile, resizedImage, true);
            }
        }
    }

    protected static byte[] Resize(string Img, int Width, int height)
    {
        //try
        //{
        System.Net.WebClient wc = new System.Net.WebClient();
        byte[] bytes = wc.DownloadData(Img);
        MemoryStream ms = new MemoryStream(bytes);

        using (System.Drawing.Image OriginalImage = System.Drawing.Image.FromStream(ms))
        {
            //if (OriginalImage.Width > Width)
            //{
                if (height <= 0)
                    height = Convert.ToInt32(Convert.ToDouble(OriginalImage.Height) / OriginalImage.Width * Width);

                using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(Width, height))
                {
                    bmp.SetResolution(OriginalImage.HorizontalResolution,
                      OriginalImage.VerticalResolution);

                    using (System.Drawing.Graphics Graphic = System.Drawing.Graphics.FromImage(bmp))
                    {
                        Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                        Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        Graphic.DrawImage(OriginalImage, 0, 0, Width, height);
                        ms = new MemoryStream();
                        bmp.Save(ms, OriginalImage.RawFormat);
                        return ms.GetBuffer();
                    }
                }
            //}
            //else
            //    return null;
        }
        //}

        //catch (Exception Ex)
        //{
        //    throw (Ex);
        //}
    } 


}