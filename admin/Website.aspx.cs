﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Website : System.Web.UI.Page
{
    protected const string ITEMNAME = "Website";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EditUsers"] == null || Convert.ToBoolean(Session["EditUsers"]) != true)
        {
            Response.Write("You don't have access to this section");
            Response.End();
            return;
        }

        divMsg.Visible = false;
        if (!IsPostBack)
        {
            FillGV();
        }

    }

    private void FillGV()
    {
        SqlDataReader dt = DB.DbFunctions.GetWebSites();

        gv.DataSource = dt;
        gv.DataBind();
    }

    protected void GvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gv.DataKeys[e.RowIndex]["ID"].ToString());

        //DB.DbFunctions.DeleteAdmin(id);

        divMsg.InnerHtml = "Selected " + ITEMNAME + " deleted successfully.";
        divMsg.Visible = true;

        FillGV();
    }
}