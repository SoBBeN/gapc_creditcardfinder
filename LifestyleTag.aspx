﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LifestyleTag.aspx.cs" Inherits="LifestyleTag" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="/css/lifestyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />
    <br />
    <div id="category">
        <asp:Literal runat="server" ID="litcategory"></asp:Literal>
    </div>
    <br />
    <div id="lifestyles">
        <asp:Literal runat="server" ID="litArticles"></asp:Literal>
    </div>
    <div id="poPrevNext">
        <div class="prev"><a runat="server" id="lnkPrev" class="lnk" visible="false">< previous</a></div>
        <div class="next"><a runat="server" id="lnkNext" class="lnk" visible="false">next ></a></div>
        <div style="clear:both;"></div>
    </div>
    <br /><br />
</asp:Content>