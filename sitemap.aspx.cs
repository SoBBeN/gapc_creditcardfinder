﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;

public partial class sitemap : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder sb = new StringBuilder();
        SqlDataReader dr = DB.DbFunctions.GetContentCategories();

        if (dr != null)
        {
            if (dr.HasRows)
            {
                int i = 0;
                sb.Append("<div class=\"row\">");
                
                while (dr.Read())
                {
                    if (i % 3 == 0)
                    {
                        sb.Append("</div>");
                        sb.Append("<div class=\"row\">");
                    }

                    sb.Append("<div class=\"sections\">");
                    sb.AppendFormat("<p><h3><a href=\"{0}\">{1}</a>", "/Content/" + Functions.StrToURL(Convert.ToString(dr["ID"])) + "/" + Functions.StrToURL(Convert.ToString(dr["Category"])) + "/", Convert.ToString(dr["Category"]) + "</h3></p>");
                    sb.Append("</div>");

                    i++;
                }

                sb.Append("</div>");

                litCategories.Text = sb.ToString();
            }
        }

        dr.Close();
        dr.Dispose();
    }
}