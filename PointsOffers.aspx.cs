﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class PointsOffers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("Find Credit Cards", "/Products/");
        int id;

        if (!int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
        {
            id = int.MinValue;
        }


        SqlDataReader dr = DB.DbFunctions.GetPointsOffers(id);

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                id = Convert.ToInt32(dr["ID"]);

                ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Functions.ConvertToString(dr["ImageFilename"]);
                breadcrumbs1.AddLevel(Convert.ToString(dr["Title"]), "/Products/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/");
                fbComments1.Href = System.Configuration.ConfigurationManager.AppSettings["baseurl"] + "Products/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["Title"]);
                ((ITmgMasterPage)Master).PageURL = "Products/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                ((ITmgMasterPage)Master).PageType = "article";
                ((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["Text"]);

                litTitle.Text = Functions.ConvertToString(dr["Title"]);
                litDate.Text = Functions.UppercaseFirst(Convert.ToDateTime(dr["DateIN"]).ToString("MMMM d, yyyy"));
                imgTopCoupon.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Functions.ConvertToString(dr["ImageFilename"]);

                litText.Text = Functions.ConvertToString(dr["Text"]);

                lnkBtn.HRef = Functions.ConvertToString(dr["URL"]);
                lnkCpnImg.HRef = lnkBtn.HRef;
                lnkCpnTitle.HRef = lnkBtn.HRef;
            }
        }

        dr = DB.DbFunctions.GetPointsOffersCategories(id);
        if (dr != null)
        {
            if (dr.HasRows)
            {
                StringBuilder sb = new StringBuilder();
                while (dr.Read())
                {
                    if (sb.Length > 0)
                        sb.Append(", ");
                    sb.Append(dr["Description"]);
                }
                litTags.Text = sb.ToString();
            }
            dr.Close();
            dr.Dispose();
        }

        dr = DB.DbFunctions.GetPointsOffersPrevNext(id);
        if (dr != null)
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    HtmlAnchor lnk;
                    HtmlAnchor imgLnk;
                    HtmlImage img;

                    if (Convert.ToString(dr["Type"]) == "Prev")
                    {
                        lnk = lnkPrev;
                        imgLnk = imgLnkPrev;
                        img = imgPrev;
                    }
                    else
                    {
                        lnk = lnkNext;
                        imgLnk = imgLnkNext;
                        img = imgNext;
                    }

                    img.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Functions.ConvertToString(dr["Thumbnail"]);
                    lnk.HRef = "/Products/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    imgLnk.HRef = lnk.HRef;
                }
            }
            dr.Close();
            dr.Dispose();
        }
    }
}